function COUNTER(divObj, initNum) {
    var template = "<button type='button'>minute</button><span></span><button type='button'>add</button>";
    divObj.innerHTML = template;
    
    var minusBtn = divObj.children[0];
    var label = divObj.children[1];
    var addBtn = divObj.children[2];

    // 要在当前插件的实例中维护的状态
    var value = 0;
    // 如果用户传入了初始化值，就把当前状态赋值为初始化值。
    if (null != initNum && undefined != initNum) {
        value = initNum;
    }

    // 在初始化过程中显示当前状态。
    label.innerHTML = value;

    // 减法按钮的事件响应。
    minusBtn.onclick = function(){
        value --;
        label.innerHTML = value;
    }

    // 加法按钮的事件响应。
    addBtn.onclick = function(){
        value ++;
        label.innerHTML = value;
    }
}