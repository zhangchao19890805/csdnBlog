import store from "./lib/store.js";
import zcBroadcast from "./lib/zcBroadcast.js";

// app.js
App({
    onLaunch() {
        // 展示本地存储能力
        const logs = wx.getStorageSync('logs') || []
        logs.unshift(Date.now())
        wx.setStorageSync('logs', logs)

        // 登录
        wx.login({
            success: res => {
                // 发送 res.code 到后台换取 openId, sessionKey, unionId

                // 这里用setTimeout 模拟发送请求获取底部菜单列表
                setTimeout(function(){
                    const app = getApp();
                    console.log(app.store.state)
                    app.store.state.tabList = [
                        {
                            "name":"首页","iconPath":"/image/icon_API.png", 
                            "selectedIconPath": "/image/icon_API_HL.png"    ,"url":"/pages/index/index"
                        },
                        {
                            "name":"页面2","iconPath":"/image/icon_component.png",
                            "selectedIconPath":"/image/icon_component_HL.png", "url":"/pages/page2/page2"
                        }
                    ]
                    app.zcBroadcast.broadcast("changeTabList");
                }, 2000)
            }
        })
    },
    globalData: {
        userInfo: null
    },

    store: store,
    zcBroadcast: zcBroadcast
})
