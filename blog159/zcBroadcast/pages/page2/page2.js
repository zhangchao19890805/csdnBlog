// pages/page2/page2.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        grade: "",
        classList: [],
        addClassName: ""
    },

    bindKeyInput(e) {
        const app = getApp();
        app.store.state.grade = e.detail.value;
        this.setData({
            grade: app.store.state.grade
        });
        app.zcBroadcast.broadcast("changeGrade")
    },

    bindClassInput(e) {
        this.setData({
            addClassName: e.detail.value
        });
    },

    clickAdd(e){
        let classList = this.data.classList;
        let addClassName = this.data.addClassName;
        classList.push(addClassName);
        this.setData({
            classList: classList,
            addClassName: ""
        });
    },

    clickDel(e){
        let classList = this.data.classList;
        classList.pop();
        this.setData({
            classList: classList
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})