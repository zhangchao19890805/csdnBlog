// custom-tab-bar/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        selected:0,
        tabList: []
    },

    lifetimes: {
        created() {
            const app = getApp();
            let self = this;
            let zcBroadcast = app.zcBroadcast;
            let listenerId = zcBroadcast.genId();
            let listener = {
                id: listenerId,
                self: self,
                action: function(text, attachment) {
                    if ("selectedTab" == text) {
                        listener.self.setData({
                            selected: app.store.state.selectedTab
                        })
                    } else if ("changeTabList" == text) {
                        listener.self.setData({
                            tabList: app.store.state.tabList
                        })
                    }
                }
            }
            zcBroadcast.addListener(listener);
        },

        attached() {
            const app = getApp();
            this.setData({
                selected: app.store.state.selectedTab,
                tabList: app.store.state.tabList
            });
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        switchTab(e) {
            const app = getApp();
            const data = e.currentTarget.dataset
            let index = Number(data.index);
            const url = this.data.tabList[index].url;
            this.setData({
                selected: index
            })
            app.store.state.selectedTab = index;
            app.zcBroadcast.broadcast("selectedTab", null);
            wx.switchTab({url})

        }
    }
})
