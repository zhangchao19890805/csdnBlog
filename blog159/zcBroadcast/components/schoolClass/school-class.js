// components/schoolClass/school-class.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        className: {
            type: String,
            value: ""
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        grade: "",
        listenerId: ""
    },

    lifetimes: {
        attached(){
            const app = getApp();
            // 添加监听器
            let zcBroadcast = app.zcBroadcast;
            let listenerId = zcBroadcast.genId();
            let self = this;
            let listener = {
                id: listenerId,
                self: self,
                action: function(text, attachment) {
                    if ("changeGrade" == text) {
                        console.log(listener.self.data.className)
                        listener.self.setData({
                            grade: app.store.state.grade
                        })
                    }
                }
            }
            zcBroadcast.addListener(listener);

            this.setData({
                listenerId: listenerId,
                grade: app.store.state.grade
            })
        }, // end attached

        detached(){
            let listenerId = this.data.listenerId;
            const app = getApp();
            app.zcBroadcast.delListener(listenerId);
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
