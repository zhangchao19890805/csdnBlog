/**
 * 广播模块
 * @author 张超
 */
let zcBroadcast = {
    // 监听器数组
    listenerList: [],

    /**
     * 生成ID
     * @author 张超
     */
    genId() {
        let t = new Date().getTime();
        let n = Math.floor(Math.random() * 1000)
        let id = t + "genid" + n;
        return id;
    },

    /**
     * 添加监听器
     * @param {object} listener 监听器对象
     * @author 张超
     * listener 中必须有id，用来标识监听器。
     * listener 中必须有 action 函数，它是监听到消息后的响应函数。
     * listener 中的 self，不是必须有，用户可以用来保存当前执行函数中的 this 。
     */
    addListener(listener) {
        if (typeof listener.id !== "string") {
            throw new Error("listener.id must be string!");
        }
        if (typeof listener.action !== "function") {
            throw new Error("listener.action must be function");
        }
        // 如果监听器id重复，删除以前的监听器
        let index = zcBroadcast.listenerList.findIndex((e, i, a) => {
            return listener.id === e.id;
        });
        if (index > -1) {
            zcBroadcast.delListener(listener.id);
        }
        zcBroadcast.listenerList.push(listener);
    },

    /**
     * 删除监听器
     * @param {string} listenerId 监听器ID
     * @author 张超
     */
    delListener(listenerId) {
        let length = zcBroadcast.listenerList.length;
        for (let i = length - 1; i >= 0; i--) {
            let item = zcBroadcast.listenerList[i];
            if (item.id === listenerId) {
                zcBroadcast.listenerList.splice(i);
            }
        }
    },

    /**
     * 广播消息
     * @param {string} text 要广播的文字消息
     * @param {object} attachment 附加信息，可以是对象，方便用户传递结构化的信息。
     * @author 张超
     */
    broadcast(text, attachment) {
        zcBroadcast.listenerList.forEach(function(e, i, a){
            e.action(text, attachment);
        })
    }
}

export default zcBroadcast; 