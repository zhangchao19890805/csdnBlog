#include "pch.h"
#include "FileUtils.h"

#include <stdlib.h>
#include <string>
#include <stdio.h>
#include<io.h>


int FileUtils::getChildrenLength(char* inDirPath) {
    int result = 0;
    //文件存储信息结构体 
    struct _finddata_t fileinfo;
    //保存文件句柄 
    intptr_t fHandle;

    if ((fHandle = _findfirst(inDirPath, &fileinfo)) == -1L)
    {
        result = 0;
    }
    else {
        do {
            result++;
        } while (_findnext(fHandle, &fileinfo) == 0);
    }
    //关闭文件 
    _findclose(fHandle);
    printf("文件数量：%d\n", result);
    return result;
}

void FileUtils::readDir(char* inDirPath, char*** outFileList, int* outFileListLength) {
    int fileListLength = FileUtils::getChildrenLength(inDirPath);
    (*outFileListLength) = fileListLength;
    if (fileListLength > 0) {
        (*outFileList) = new char* [fileListLength];
        //文件存储信息结构体 
        struct _finddata_t fileinfo;
        //保存文件句柄 
        intptr_t fHandle;
        //文件数记录器
        int i = 0;

        if ((fHandle = _findfirst(inDirPath, &fileinfo)) == -1L)
        {
            printf("当前目录下没有txt文件\n");
        }
        else {
            do {

                int fileNameSize = int(strlen(fileinfo.name));
                char* fileName = new char[fileNameSize + 1];
                for (int fileNameIndex = 0; fileNameIndex < fileNameSize; fileNameIndex++) {
                    char c = fileinfo.name[fileNameIndex];
                    fileName[fileNameIndex] = c;
                }
                fileName[fileNameSize] = '\0';
                (*outFileList)[i] = fileName;
                i++;
                //printf("找到文件:%s,文件大小：%d\n", fileinfo.name, fileinfo.size);
            } while (_findnext(fHandle, &fileinfo) == 0);
        }
        //关闭文件 
        _findclose(fHandle);
    }
} // end void FileUtils::readDir


char* FileUtils::getFileMainName(char* fileName) {
    int len = int(strlen(fileName));
    int max = 0;
    for (int i = 0; i < len; i++) {
        char c = fileName[i];
        if ('.' == c) {
            break;
        }
        else {
            max++;
        }
    }
    if (max <= 0) {
        return NULL;
    }
    max++;

    char* result = new char[max];
    for (int i = 0; i < len; i++) {
        char c = fileName[i];
        if ('.' == c) {
            break;
        }
        else {
            result[i] = c;
        }
    }
    result[max - 1] = '\0';
    return result;
}

int FileUtils::findIndex(char** picList, const int picListLength, const char* xmlFileMainName) {
    int result = -1;
    for (int i = 0; i < picListLength; i++) {
        char* picFileMainName = getFileMainName(picList[i]);
        if (NULL != picFileMainName && 0 == strcmp(xmlFileMainName, picFileMainName)) {
            result = i;
        }
        delete[] picFileMainName;
    }
    return result;
}

int FileUtils::getXmlPicLength(char** xmlList, int xmlListLength, char** picList, int picListLength) {
    int result = 0;
    for (int xmlIndex = 0; xmlIndex < xmlListLength; xmlIndex++) {
        char* xmlFileName = xmlList[xmlIndex];
        char* xmlFileMainName = getFileMainName(xmlFileName);

        if (findIndex(picList, picListLength, xmlFileMainName) > -1) {
            result++;
        }

        delete[] xmlFileMainName;
    }
    return result;
}

int FileUtils::readXmlPic(char**** outFaceList, int* outFaceListLength) {
    // xml文件名列表
    char xmlDirExp[] = "D:\\your_xml\\*.xml";
    char** xmlList = NULL;
    int xmlListLength = 0;
    readDir(xmlDirExp, &xmlList, &xmlListLength);

    // 图片文件名列表
    char picDirExp[] = "D:\\your_img\\*.*";
    char** picList = NULL;
    int picListLength = 0;
    readDir(picDirExp, &picList, &picListLength);

    if (NULL == xmlList || NULL == picList) {
        return 0;
    }

    int xmlPicListLength = getXmlPicLength(xmlList, xmlListLength, picList, picListLength);
    (*outFaceListLength) = xmlPicListLength;
    if (xmlPicListLength > 0) {
        (*outFaceList) = new char** [xmlPicListLength];
        for (int indexLv1 = 0; indexLv1 < xmlPicListLength; indexLv1++) {
            (*outFaceList)[indexLv1] = new char* [2];
        }

        // 用于拼接xml和图片文件的全名
        char xmlDirPath[] = "D:\\your_xml\\";
        char picDirPath[] = "D:\\your_img\\";

        int faceIndex = 0;
        for (int xmlIndex = 0; xmlIndex < xmlListLength; xmlIndex++) {
            char* xmlFileName = xmlList[xmlIndex];
            char* xmlFileMainName = getFileMainName(xmlFileName);

            // 找到同名的图片
            int index = findIndex(picList, picListLength, xmlFileMainName);
            if (index > -1) {
                char* picFileName = picList[index];
                if (faceIndex >= xmlPicListLength) {
                    return -1;
                }

                size_t xmlFullMax = strlen(xmlDirPath) + strlen(xmlFileName) + 1;
                (*outFaceList)[faceIndex][0] = new char[xmlFullMax];
                size_t picFullMax = strlen(picDirPath) + strlen(picFileName) + 1;
                (*outFaceList)[faceIndex][1] = new char[picFullMax];

                sprintf_s((*outFaceList)[faceIndex][0], xmlFullMax, "%s%s", xmlDirPath, xmlFileName);
                sprintf_s((*outFaceList)[faceIndex][1], picFullMax, "%s%s", picDirPath, picFileName);

                faceIndex++;
            }

            delete[] xmlFileMainName;
        }
    }


    if (NULL != xmlList) {
        for (int i = 0; i < xmlListLength; i++) {
            if (NULL != xmlList[i]) {
                delete[] xmlList[i];
                xmlList[i] = NULL;
            }
        }
        delete[] xmlList;
        xmlList = NULL;
    }
    if (NULL != picList) {
        for (int i = 0; i < picListLength; i++) {
            if (NULL != picList[i]) {
                delete[] picList[i];
                picList[i] = NULL;
            }
        }
        delete[] picList;
        picList = NULL;
    }
    return 1;
}


