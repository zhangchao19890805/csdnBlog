#include "pch.h"
#include "FDLibCapabilities.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "HkXmlResult.h"
#include "Lexer.h"
#include "Token.h"
#include "TagNameIdConst.h"
#include "TokenTypeConst.h"


using namespace std;


bool FDLibCapabilities::getCapabilities(int lUserID, HkXmlResult* hkXmlResult)
{
	bool result = false;

	// 组装输入参数
	char requestUrlArr[64];
	sprintf_s(requestUrlArr, "GET /ISAPI/Intelligent/FDLib/capabilities");
	NET_DVR_XML_CONFIG_INPUT inputParam = { 0 };
	inputParam.dwSize = sizeof(inputParam);
	inputParam.lpRequestUrl = requestUrlArr;
	inputParam.dwRequestUrlLen = (unsigned long)strlen(requestUrlArr);

	// 输出参数
	NET_DVR_XML_CONFIG_OUTPUT outputParam = { 0 };
	outputParam.dwSize = sizeof(outputParam);
	outputParam.lpOutBuffer = hkXmlResult->xml;
	outputParam.dwOutBufferSize = hkXmlResult->xmlArrLength;

	BOOL flag = NET_DVR_STDXMLConfig(lUserID, &inputParam, &outputParam);

	if (flag) {
		hkXmlResult->xmlSize = outputParam.dwReturnedXMLSize;
		result = true;
	}
	return result;
}


bool FDLibCapabilities::getIsSupportStandardSearch(HkXmlResult* hkXmlResult) {
	bool result = false;

	Token* tokenArr = NULL;
	int tokenArrSize = 0;
	Lexer::generateTokenList(hkXmlResult->xml, hkXmlResult->xmlSize, &tokenArr, &tokenArrSize);

	if (NULL != tokenArr) {
		int beginIndex_isSupportStandardSearch = -1;
		int   endIndex_isSupportStandardSearch = -1;
		for (int i = 0; i < tokenArrSize; i++) {
			Token token = tokenArr[i];
			if (TokenTypeConst::BeginTagToken == token.tokenType && TagNameIdConst::isSupportStandardSearch == token.tagNameId) {
				beginIndex_isSupportStandardSearch = token.endIndex + 1;
			}
			else if (TokenTypeConst::EndTagToken == token.tokenType && TagNameIdConst::isSupportStandardSearch == token.tagNameId) {
				endIndex_isSupportStandardSearch = token.beginIndex - 1;
			}
		}
		if (beginIndex_isSupportStandardSearch > 0 && endIndex_isSupportStandardSearch > 0) {
			for (int i = beginIndex_isSupportStandardSearch; i <= endIndex_isSupportStandardSearch; i++) {
				char ch = hkXmlResult->xml[i];
				if (Lexer::isWhitespace(ch)) {
					beginIndex_isSupportStandardSearch++;
				}
			}
			if (beginIndex_isSupportStandardSearch <= endIndex_isSupportStandardSearch) {
				for (int i = endIndex_isSupportStandardSearch; i >= beginIndex_isSupportStandardSearch; i--) {
					char ch = hkXmlResult->xml[i];
					if (Lexer::isWhitespace(ch)) {
						endIndex_isSupportStandardSearch--;
					}
				}
				if ((endIndex_isSupportStandardSearch - beginIndex_isSupportStandardSearch + 1) == 4) {
					char ch0 = hkXmlResult->xml[beginIndex_isSupportStandardSearch];
					char ch1 = hkXmlResult->xml[beginIndex_isSupportStandardSearch + 1];
					char ch2 = hkXmlResult->xml[beginIndex_isSupportStandardSearch + 2];
					char ch3 = hkXmlResult->xml[beginIndex_isSupportStandardSearch + 3];
					if ('t' == ch0 && 'r' == ch1 && 'u'==ch2 && 'e'==ch3) {
						result = true;
					}
				}
			}
		}
	
		delete[] tokenArr;
	}
	return result;
}