#include "pch.h"
#include "UploadPicture.h"
#include <Windows.h>
// https://open.hikvision.com/hardware/v2/XML%E6%96%87%E4%BB%B6/XML_FaceAppendData.html

#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "FileUtils.h"
#include "StringBuilder.h"
#include "Logger.h"

using namespace std;


LONG UploadPicture::getUploadState(long m_lUploadHandle)
{
	unique_ptr<StringBuilder> sb(new StringBuilder());
	unique_ptr<Logger> logger(new Logger());

	DWORD dwProgress = 0;
	LONG iStatus = -1;
	char szStatus[256] = { 0 };

	memset(szStatus, 0, 256);
	iStatus = NET_DVR_GetUploadState(m_lUploadHandle, &dwProgress);
	if (iStatus == -1)  //即入口返回失败
	{
		printf("上传失败!status[%d]", iStatus);
		sb->clear()->appendGbkToUtf8("上传失败")->append("!status=")->append(iStatus);
		logger->info(sb.get());
		Sleep(100);
	}
	else if (iStatus == 2) //正在上传
	{
		printf("正在上传!status[%d],progress[%d]", iStatus, dwProgress);
		sb->clear()->appendGbkToUtf8("正在上传")->append("!status=")->append(iStatus)->append(",  progress=")->append(dwProgress);
		logger->info(sb.get());
		Sleep(100);
	}
	else if (iStatus == 1) //上传成功
	{
		printf("上传成功!status[%d],progress[%d]", iStatus, dwProgress);
		logger->info(sb->clear()->appendGbkToUtf8("上传成功")->append("!status=")->append(iStatus)->append(",  progress=")->append(dwProgress));
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 35)
	{
		printf("图片URL未开始下载");
		logger->info(sb->clear()->appendGbkToUtf8("图片")->append("URL")->appendGbkToUtf8("未开始下载"));
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 36)
	{
		printf("customHumanID重复");
		sb->clear()->append("customHumanID")->appendGbkToUtf8("重复");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 38)
	{
		printf("建模失败,设备内部错误");
		sb->clear()->appendGbkToUtf8("建模失败,设备内部错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 39)
	{
		printf("建模失败,人脸建模错误");
		sb->clear()->appendGbkToUtf8("建模失败,人脸建模错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 40)
	{
		printf("建模失败,人脸质量评分错误");
		sb->clear()->appendGbkToUtf8("建模失败,人脸质量评分错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 41)
	{
		printf("建模失败,特征点提取错误");
		sb->clear()->appendGbkToUtf8("建模失败,特征点提取错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 42)
	{
		printf("建模失败,属性提取错误");
		sb->clear()->appendGbkToUtf8("建模失败,属性提取错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 43)
	{
		printf("图片数据错误");
		sb->clear()->appendGbkToUtf8("图片数据错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else if (iStatus == 44)
	{
		printf("图片附加信息错误");
		sb->clear()->appendGbkToUtf8("图片附加信息错误");
		logger->info(sb.get());
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	else //上传失败,显示状态
	{
		printf("上传失败!status[%d]", iStatus);
		logger->info(sb->clear()->appendGbkToUtf8("上传失败")->append("!status=")->append(iStatus));
		Sleep(100); //这个sleep只是为了addlog能够有时间执行
	}
	return iStatus;
}


void UploadPicture::SetUserFace(int lUserID, int n, char* fdid, char* xmlFileName, char* picFileName)
{
	unique_ptr<StringBuilder> sb(new StringBuilder());
	unique_ptr<Logger> logger(new Logger());

	NET_DVR_FACELIB_COND struFaceLibCond = { 0 };
	struFaceLibCond.dwSize = sizeof(NET_DVR_FACELIB_COND);
	struFaceLibCond.byConcurrent = 0;
	struFaceLibCond.byCover = 0;
	struFaceLibCond.byCustomFaceLibID = 0;
	memcpy(struFaceLibCond.szFDID, fdid, strlen(fdid));

	LONG m_lUploadHandle = NET_DVR_UploadFile_V40(lUserID, IMPORT_DATA_TO_FACELIB, &struFaceLibCond, sizeof(NET_DVR_FACELIB_COND), NULL, NULL, 0);
	if (m_lUploadHandle < 0)
	{
		sb->clear()->appendGbkToUtf8("IMPORT_DATA_TO_FACELIB建立下发人脸的长连接失败，错误码为:")->append(NET_DVR_GetLastError());
		logger->info(sb.get());
		return;
	}
	else
	{
		NET_DVR_SEND_PARAM_IN m_struSendParam = { 0 };

		memset(&m_struSendParam, 0, sizeof(m_struSendParam));

		char szLan[1280] = { 0 };
		char szFileName[MAX_PATH] = { 0 };
		DWORD dwFileSize = 0;

		for (int i = 0; i < n; i++)
		{
			//读XML文件
			FILE* fp = NULL;
			BYTE* byteBuffer = NULL;
			int xmlSize = 0;
			
			errno_t err;
			err = fopen_s(&fp, xmlFileName, "rb");
			if (0 == err)
			{
				//求得文件的大小  
				fseek(fp, 0, SEEK_END);
				xmlSize = ftell(fp);
				rewind(fp);
				byteBuffer = new BYTE[xmlSize + 1];
				fread(byteBuffer, xmlSize, 1, fp);
				m_struSendParam.pSendAppendData = byteBuffer;
				m_struSendParam.dwSendAppendDataLen = xmlSize;
				printf("读取XML成功，XML大小：%dByte\n", xmlSize);
			}
			if (NULL != fp) {
				fclose(fp);
			}
			fp = NULL;

			//unsigned char* pSendAppendData = new unsigned char[1024];
			//sprintf_s(pSendAppendData, "aa");
			//m_struSendParam.pSendAppendData = pSendAppendData;
			//m_struSendParam.dwSendAppendDataLen = xmlSize;

			//读图片文件
			errno_t err2;
			FILE* fpFace = NULL;
			BYTE* byteFaceBuffer = NULL;
			int picSize = 0;
			err2 = fopen_s(&fpFace, picFileName, "rb");
			if (0 == err2)
			{
				//求得文件的大小  
				fseek(fpFace, 0, SEEK_END);
				picSize = ftell(fpFace);
				rewind(fpFace);
				byteFaceBuffer = new BYTE[picSize + 1];
				fread(byteFaceBuffer, picSize, 1, fpFace);
				m_struSendParam.pSendData = byteFaceBuffer;
				m_struSendParam.dwSendDataLen = picSize;
				m_struSendParam.byPicType = 1;
				printf("读取图片成功，图片大小：%dByte\n", picSize);
			}

			SYSTEMTIME sysTime = { 0 };
			GetLocalTime(&sysTime);
			m_struSendParam.struTime.wYear = (unsigned char)sysTime.wYear;
			m_struSendParam.struTime.byMonth = (unsigned char)sysTime.wMonth;
			m_struSendParam.struTime.byDay = (unsigned char)sysTime.wDay;
			m_struSendParam.struTime.byHour = (unsigned char)sysTime.wHour;
			m_struSendParam.struTime.byMinute = (unsigned char)sysTime.wMinute;
			m_struSendParam.struTime.bySecond = (unsigned char)sysTime.wSecond;
			m_struSendParam.struTime.wMilliSec = (unsigned char)sysTime.wMilliseconds;
			if (NULL != fpFace) {
				fclose(fpFace);
			}
			fpFace = NULL;

			if (NET_DVR_UploadSend(m_lUploadHandle, &m_struSendParam, NULL) < 0)
			{
				cout << "NET_DVR_UploadSend失败，错误码为:" << NET_DVR_GetLastError() << endl;
				sb->clear()->append("NET_DVR_UploadSend")->appendGbkToUtf8("失败，错误码为：")->append(NET_DVR_GetLastError());
			}
			else
			{
				for (int tryNum = 0; tryNum < 20; tryNum++)
				{
					LONG iStatus = UploadPicture::getUploadState(m_lUploadHandle);
					if (1 == iStatus)
					{
						NET_DVR_UPLOAD_FILE_RET m_struPicRet = { 0 };
						NET_DVR_GetUploadResult(m_lUploadHandle, &m_struPicRet, sizeof(m_struPicRet));
						break;
					}
					else if ((iStatus >= 3 && iStatus <= 10) || iStatus == 31 || iStatus == -1)
					{
						break;
					}
				}
				cout << "NET_DVR_UploadSend下发结束，个数：" << (i + 1) << endl;
			}
		} // end for 

		if (!NET_DVR_UploadClose(m_lUploadHandle))
		{
			cout << "NET_DVR_UploadClose失败，错误码为:" << NET_DVR_GetLastError() << endl;
			sb->clear()->append("NET_DVR_UploadClose")->appendGbkToUtf8("失败，错误码为：")->append(NET_DVR_GetLastError());
			logger->info(sb.get());
		}
		else
		{
			cout << "NET_DVR_UploadClose成功";
			sb->clear()->append("NET_DVR_UploadClose")->appendGbkToUtf8("成功");
			logger->info(sb.get());
		}
	}
}


