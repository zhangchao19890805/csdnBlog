#pragma once
class TagNameIdConst
{
public:
	static const int XML_FILE_TAG = 100;
	static const int isSupportStandardSearch = 101;
	static const int FDID = 102;
	static const int MatchElement = 103;
	static const int PID = 104;
	static const int ModelingStatus = 105;
	static const int totalMatches = 106;
	
	static const int OTHER = 1000;
};

