#pragma once
#include "HkXmlResult.h"
#include "Token.h"

class FDSearch
{

private:
	static int searchID;

	static int getPictureIdListLength(Token* tokenArr, int tokenArrSize);

public:
	// 调用 POST /ISAPI/Intelligent/FDLib/FDSearch 接口，查询人脸图片分页。
	static bool search(int lUserID, int fdlibSearchStart, char* fdid, HkXmlResult* hkXmlResult);

	// 解析XML获取图片ID列表。
	static void getPictureIdList(const HkXmlResult* hkXmlResult, char*** outPictureIdList, int* outPictureIdListLength);

	// 解析XML获取图片总数量。
	static int getPageTotal(const HkXmlResult* hkXmlResult);
};



