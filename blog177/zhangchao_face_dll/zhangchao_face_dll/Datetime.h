#pragma once
class Datetime
{
private:
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
	long long m_time;

public:
	Datetime();
	Datetime(long long utcTime);
	Datetime(int year, int month, int day, int hour, int minute, int second);
	~Datetime();


	int getYear();
	int getMonth();
	int getDay();
	int getHour();
	int getMinute();
	int getSecond();

	long long getTime();


};

