#include "pch.h"
#include "FDSearch.h"

#include <Windows.h>
#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "HkXmlResult.h"
#include "Token.h"
#include "TokenTypeConst.h"
#include "TagNameIdConst.h"
#include "Lexer.h"
#include "Logger.h"
#include "TagNameIdConst.h"
#include "StringBuilder.h"

using namespace std;


int FDSearch::searchID = 0;




bool FDSearch::search(int lUserID, int fdlibSearchStart, char* fdid, HkXmlResult* hkXmlResult) {
	unique_ptr<StringBuilder> sb(new StringBuilder());
	unique_ptr<Logger> logger(new Logger());
	FDSearch::searchID++;
	bool result = false;
	char requestUrlArr[64];
	sprintf_s(requestUrlArr, "POST /ISAPI/Intelligent/FDLib/FDSearch");
	NET_DVR_XML_CONFIG_INPUT inputParam = { 0 };
	inputParam.dwSize = sizeof(inputParam);
	inputParam.lpRequestUrl = requestUrlArr;
	inputParam.dwRequestUrlLen = (unsigned long)strlen(requestUrlArr);
	// 输入的XML
	char lpInBuffer[512];
	sprintf_s(lpInBuffer, "<FDSearchDescription version=\"2.0\" \
		xmlns=\"http://www.std-cgi.org/ver20/XMLSchema\">  \
		<searchID>%d</searchID> \
		<searchResultPosition>%d</searchResultPosition> \
		<maxResults>50</maxResults> \
		<FDID>%s</FDID> \
		<startTime>1939-12-12T00:00:00Z</startTime> \
        </FDSearchDescription>", FDSearch::searchID, fdlibSearchStart, fdid);
	inputParam.lpInBuffer = lpInBuffer;
	unsigned long dwInBufferSize = (unsigned long)strlen(lpInBuffer);
	sb->clear()->append("lpInBuffer=")->append(lpInBuffer);
	logger->info(sb.get());
	inputParam.dwInBufferSize = dwInBufferSize;

	// 输出的结构体
	NET_DVR_XML_CONFIG_OUTPUT outputParam = { 0 };
	outputParam.dwSize = sizeof(outputParam);
	outputParam.lpOutBuffer = hkXmlResult->xml;
	outputParam.dwOutBufferSize = hkXmlResult->xmlArrLength;

	std::cout << "Before NET_DVR_STDXMLConfig." << endl;
	BOOL flag = NET_DVR_STDXMLConfig(lUserID, &inputParam, &outputParam);

	if (flag) {
		hkXmlResult->xmlSize = outputParam.dwReturnedXMLSize;
		result = true;
	}

	return result;
}



int FDSearch::getPictureIdListLength(Token* tokenArr, int tokenArrSize) {
	int result = 0;

	bool inMatchElement = false;
	bool inModelingStatus = false;

	for (int i = 0; i < tokenArrSize; i++) {
		if (TagNameIdConst::MatchElement == tokenArr[i].tagNameId && TokenTypeConst::BeginTagToken == tokenArr[i].tokenType) {
			inMatchElement = true;
		}
		else if (TagNameIdConst::MatchElement == tokenArr[i].tagNameId &&
			TokenTypeConst::EndTagToken == tokenArr[i].tokenType)
		{
			inMatchElement = false;
		}
		else if (TagNameIdConst::ModelingStatus == tokenArr[i].tagNameId &&
			TokenTypeConst::BeginTagToken == tokenArr[i].tokenType)
		{
			inModelingStatus = true;
		}
		else if (TagNameIdConst::ModelingStatus == tokenArr[i].tagNameId &&
			TokenTypeConst::EndTagToken == tokenArr[i].tokenType)
		{
			inModelingStatus = false;
		}
		else if (inMatchElement && !inModelingStatus && TagNameIdConst::PID == tokenArr[i].tagNameId &&
			TokenTypeConst::BeginTagToken == tokenArr[i].tokenType && TokenTypeConst::TextToken == tokenArr[i + 1].tokenType)
		{
			result++;
		}
	}

	return result;
}

void FDSearch::getPictureIdList(const HkXmlResult* hkXmlResult, char*** outPictureIdList, int* outPictureIdListLength) {
	Token* tokenArr = NULL;
	int tokenArrSize = 0;
	Lexer::generateTokenList(hkXmlResult->xml, hkXmlResult->xmlSize, &tokenArr, &tokenArrSize);

	if (NULL != tokenArr) {
		// 先获取PID个数，这样方便确定图片ID列表的长度。
		int pictureIdListLength = FDSearch::getPictureIdListLength(tokenArr, tokenArrSize);
		(*outPictureIdListLength) = pictureIdListLength;
	
		// 如果没有PID，图片ID列表不做任何改动。
		if (pictureIdListLength > 0) {
			(*outPictureIdList) = new char* [pictureIdListLength];
			int outPictureIdListIndex = 0;
			bool inMatchElement = false;  // 是否在 MatchElement 标签对中.
			bool inModelingStatus = false; // 是否在 ModelingStatus 标签对中.
			for (int i = 0; i < tokenArrSize; i++) {
				if (TagNameIdConst::MatchElement == tokenArr[i].tagNameId && TokenTypeConst::BeginTagToken == tokenArr[i].tokenType) {
					inMatchElement = true;
				}
				else if (TagNameIdConst::MatchElement == tokenArr[i].tagNameId && 
					TokenTypeConst::EndTagToken == tokenArr[i].tokenType) 
				{
					inMatchElement = false;
				}
				else if (TagNameIdConst::ModelingStatus == tokenArr[i].tagNameId &&
					TokenTypeConst::BeginTagToken == tokenArr[i].tokenType)
				{
					inModelingStatus = true;
				}
				else if (TagNameIdConst::ModelingStatus == tokenArr[i].tagNameId &&
					TokenTypeConst::EndTagToken == tokenArr[i].tokenType)
				{
					inModelingStatus = false;
				}
				else if (inMatchElement && !inModelingStatus && TagNameIdConst::PID == tokenArr[i].tagNameId &&
					TokenTypeConst::BeginTagToken == tokenArr[i].tokenType && TokenTypeConst::TextToken == tokenArr[i + 1].tokenType)
				{
					int pidTokenIndex = i + 1;
					int pidChArrMax = tokenArr[pidTokenIndex].endIndex - tokenArr[pidTokenIndex].beginIndex + 2;
					char* pid = new char[pidChArrMax];
					for (int xmlIndex = tokenArr[pidTokenIndex].beginIndex; xmlIndex <= tokenArr[pidTokenIndex].endIndex; xmlIndex++) {
						int pidIndex = xmlIndex - tokenArr[pidTokenIndex].beginIndex;
						pid[pidIndex] = hkXmlResult->xml[xmlIndex];
					}
					pid[pidChArrMax - 1] = '\0';
					(*outPictureIdList)[outPictureIdListIndex] = pid;
					outPictureIdListIndex++;
				}
			}
		}

		delete[] tokenArr;
	}
}



int FDSearch::getPageTotal(const HkXmlResult* hkXmlResult) {
	int result = 0;
	Token* tokenArr = NULL;
	int tokenArrSize = 0;
	Lexer::generateTokenList(hkXmlResult->xml, hkXmlResult->xmlSize, &tokenArr, &tokenArrSize);
	if (NULL != tokenArr) {
		int total = 0;
		char* totalChArr = NULL;

		for (int i = 0; i < tokenArrSize; i++) {
			if (TagNameIdConst::totalMatches == tokenArr[i].tagNameId && TokenTypeConst::BeginTagToken == tokenArr[i].tokenType &&
				TokenTypeConst::TextToken == tokenArr[i+1].tokenType) 
			{
				int totalChArrMax = tokenArr[i + 1].endIndex - tokenArr[i + 1].beginIndex + 2;
				totalChArr = new char[totalChArrMax];
				for (int totalIndex = tokenArr[i + 1].beginIndex; totalIndex <= tokenArr[i + 1].endIndex; totalIndex++) {
					int beginIndex = tokenArr[i + 1].beginIndex;
					totalChArr[totalIndex - beginIndex] = hkXmlResult->xml[totalIndex];
				}
				totalChArr[totalChArrMax-1] = '\0';
			}
		}

		if (NULL != totalChArr) {
			total = atoi(totalChArr);
		}
		result = total / 50 + 1;
		if (NULL != totalChArr) {
			delete[] totalChArr;
			totalChArr = NULL;
		}
	
		delete[] tokenArr;
	}
	return result;
}
