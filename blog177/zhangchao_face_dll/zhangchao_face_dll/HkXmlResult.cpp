#include "pch.h"
#include "HkXmlResult.h"


HkXmlResult::HkXmlResult() {
	xmlSize = 0;
	xmlArrLength = 1024 * 1024;
	xml = new char[xmlArrLength];
}

HkXmlResult::~HkXmlResult() {
	delete[] xml;
}