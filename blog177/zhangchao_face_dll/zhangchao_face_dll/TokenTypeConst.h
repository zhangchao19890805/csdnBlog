#pragma once
class TokenTypeConst
{
public:
	static const int XmlFileToken = 1;
	static const int BeginTagToken = 2;
	static const int EndTagToken = 3;
	static const int SingleTagToken = 4;
	static const int TextToken = 5;
};

