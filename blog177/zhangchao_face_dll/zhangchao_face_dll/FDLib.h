#pragma once
#include "HkXmlResult.h"


class FDLib
{
public:

	static char* add(int lUserID);

	// 调用海康接口，获取所有人脸库的XML结果。
	static bool listAll(int lUserID, HkXmlResult* hkXmlResult);

	// 获取人脸库的ID，返回指向char数组的char指针。
	static char* getFDID(HkXmlResult* hkXmlResult);
};

