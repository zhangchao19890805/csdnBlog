#include "pch.h"
#include "DelPicture.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

void DelPicture::deleteById(const int lUserID, const char* fdid, const char* pid) {
	size_t fdid_size_t = strlen(fdid);
	size_t pid_size_t = strlen(pid);

	char* requestUrlArr = new char[fdid_size_t + pid_size_t + 128];
	sprintf_s(requestUrlArr, fdid_size_t + pid_size_t + 128, "DELETE /ISAPI/Intelligent/FDLib/%s/picture/%s", fdid, pid);
	NET_DVR_XML_CONFIG_INPUT inputParam = { 0 };
	inputParam.dwSize = sizeof(inputParam);
	cout << "inputParam.dwSize=" << inputParam.dwSize << endl;
	inputParam.lpRequestUrl = requestUrlArr;
	inputParam.dwRequestUrlLen = (unsigned long)strlen(requestUrlArr);

	NET_DVR_XML_CONFIG_OUTPUT outputParam = { 0 };
	outputParam.dwSize = sizeof(outputParam);
	std::cout << "outputParam.dwSize=" << outputParam.dwSize << endl;
	DWORD dwOutBufferSize = 1024 * 1024;
	char* lpOutBuffer = new char[dwOutBufferSize];
	outputParam.lpOutBuffer = lpOutBuffer;
	outputParam.dwOutBufferSize = dwOutBufferSize;

	BOOL flag = NET_DVR_STDXMLConfig(lUserID, &inputParam, &outputParam);

	delete[] requestUrlArr;
	requestUrlArr = NULL;
	delete[] lpOutBuffer;
	lpOutBuffer = NULL;
}