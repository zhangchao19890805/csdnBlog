#include "pch.h"
#include "Lexer.h"
#include<string>
#include <iostream>
#include "TagNameIdConst.h"
#include "TokenTypeConst.h"
#include "Logger.h"
#include "StringBuilder.h"

using namespace std;


int Lexer::endIndex_XmlFileToken(char* xml, int xmlSize, int startIndex) {
	int result = -1;
	int noScanSize = xmlSize - 1 - startIndex;
	if (noScanSize < 8) {
		return result;
	}
	char beginArr[5] = {'<','?','x','m','l'};
	for (int i = 0; i < 5; i++) {
		char ch = beginArr[i];
		if (ch != xml[startIndex + i]) {
			return result;
		}
	}
	// 第五个字符应该是空白字符串
	char ch5 = xml[startIndex + 5];
	if (' ' != ch5 && '\r' != ch5 && '\n' != ch5 && '\t' != ch5) {
		return result;
	}
	
	bool isInDoubleQuote = false; // 是否在双括号中。
	bool isInSingleQuote = false; // 是否在单括号中。

	for (int i = startIndex + 6; i < xmlSize - 1; i++) {
		char ch = xml[i];
		char next = xml[i + 1];
		// 开启双引号
		if (!isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = true;
		}
		// 开启单引号
		else if (!isInDoubleQuote && !isInSingleQuote && '\'' == ch) {
			bool isInSingleQuote = true;
		}
		// 双引号中避免反斜杠转义符的干扰
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\\' == next) {
			i ++;
		}
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\"' == next) {
			i++;
		}
		// 单引号中避免反斜杠转义符的干扰
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\'' == next) {
			i++;
		}
		// 关闭双引号
		else if (isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = false;
		}
		// 关闭单引号
		else if (!isInDoubleQuote && isInSingleQuote && '\'' == ch) {
			isInSingleQuote = false;
		}
		else if (!isInDoubleQuote && !isInSingleQuote && '?' == ch && '>' == next) {
			return (i + 1);
		}
	}
	return result;
}



int Lexer::endIndex_BeginTagToken(char* xml, int xmlSize, int startIndex) {
	int noScanSize = xmlSize - 1 - startIndex;
	if (noScanSize < 3) {
		return -1;
	}
	if ('<' != xml[startIndex]) {
		return -1;
	}
	// 结束标签
	if ('<' == xml[startIndex] && '/' == xml[startIndex + 1]) {
		return -1;
	}

	bool isInDoubleQuote = false; // 是否在双括号中。
	bool isInSingleQuote = false; // 是否在单括号中。

	for (int i = startIndex + 1; i < xmlSize; i++) {
		char ch = xml[i];
		char next;
		if (i < xmlSize - 1) {
			next = xml[i + 1];
		}
		else {
			next = ' ';
		}
		// 开启双引号
		if (!isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = true;
		}
		// 开启单引号
		else if (!isInDoubleQuote && !isInSingleQuote && '\'' == ch) {
			bool isInSingleQuote = true;
		}
		// 双引号中避免反斜杠转义符的干扰
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\"' == next) {
			i++;
		}
		// 单引号中避免反斜杠转义符的干扰
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\'' == next) {
			i++;
		}
		// 关闭双引号
		else if (isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = false;
		}
		// 关闭单引号
		else if (!isInDoubleQuote && isInSingleQuote && '\'' == ch) {
			isInSingleQuote = false;
		}
		// 单标签
		else if (!isInDoubleQuote && !isInSingleQuote && '/' == ch && '>' == next) {
			return -1;
		}
		else if (!isInDoubleQuote && !isInSingleQuote && '>' == ch) {
			return i;
		}
	}
	
	return -1;
}


bool Lexer::startWith(char* xml, int startIndex, int endIndex, char tagName[]) {
	const int tagLen = int(strlen(tagName));
	int rightNum = 0;
	for (int i = 0; i < tagLen && (startIndex + i) <= endIndex; i++) {
		char xmlCh = xml[startIndex + i];
		char tagCh = tagName[i];
		if (xmlCh == tagCh) {
			rightNum++;
		}
	}
	if (tagLen == rightNum) {
		return true;
	}
	return false;
}

int Lexer::getTagNameId(char* xml, int startIndex, int endIndex) {
	char isSupportStandardSearch_begin[] = "<isSupportStandardSearch";
	if (startWith(xml, startIndex, endIndex, isSupportStandardSearch_begin)) {
		int chIndex = startIndex + int(strlen(isSupportStandardSearch_begin));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::isSupportStandardSearch;
		}
	}

	char isSupportStandardSearch_end[] = "</isSupportStandardSearch";
	if (startWith(xml, startIndex, endIndex, isSupportStandardSearch_end)) {
		int chIndex = startIndex + int(strlen(isSupportStandardSearch_end));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::isSupportStandardSearch;
		}
	}

	char FDID_begin[] = "<FDID";
	if (startWith(xml, startIndex, endIndex, FDID_begin)) {
		int chIndex = startIndex + int(strlen(FDID_begin));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::FDID;
		}
	}

	char FDID_end[] = "</FDID";
	if (startWith(xml, startIndex, endIndex, FDID_end)) {
		int chIndex = startIndex + int(strlen(FDID_end));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::FDID;
		}
	}

	char MatchElement_begin[] = "<MatchElement";
	if (startWith(xml, startIndex, endIndex, MatchElement_begin)) {
		int chIndex = startIndex + int(strlen(MatchElement_begin));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::MatchElement;
		}
	}

	char MatchElement_end[] = "</MatchElement";
	if (startWith(xml, startIndex, endIndex, MatchElement_end)) {
		int chIndex = startIndex + int(strlen(MatchElement_end));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::MatchElement;
		}
	}

	char PID_begin[] = "<PID";
	if (startWith(xml, startIndex, endIndex, PID_begin)) {
		int chIndex = startIndex + int(strlen(PID_begin));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::PID;
		}
	}

	char PID_end[] = "</PID";
	if (startWith(xml, startIndex, endIndex, PID_end)) {
		int chIndex = startIndex + int(strlen(PID_end));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::PID;
		}
	}

	char ModelingStatus_begin[] = "<ModelingStatus";
	if (startWith(xml, startIndex, endIndex, ModelingStatus_begin)) {
		int chIndex = startIndex + int(strlen(ModelingStatus_begin));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::ModelingStatus;
		}
	}

	char ModelingStatus_end[] = "</ModelingStatus";
	if (startWith(xml, startIndex, endIndex, ModelingStatus_end)) {
		int chIndex = startIndex + int(strlen(ModelingStatus_end));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::ModelingStatus;
		}
	}

	char totalMatches_begin[] = "<totalMatches";
	if (startWith(xml, startIndex, endIndex, totalMatches_begin)) {
		int chIndex = startIndex + int(strlen(totalMatches_begin));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::totalMatches;
		}
	}

	char totalMatches_end[] = "</totalMatches";
	if (startWith(xml, startIndex, endIndex, totalMatches_end)) {
		int chIndex = startIndex + int(strlen(totalMatches_end));
		char ch = xml[chIndex];
		if ('>' == ch || isWhitespace(ch)) {
			return TagNameIdConst::totalMatches;
		}
	}
	return TagNameIdConst::OTHER;
}


int Lexer::endIndex_EndTagToken(char* xml, int xmlSize, int startIndex) {
	int noScanSize = xmlSize - 1 - startIndex;
	if (noScanSize < 4) {
		return -1;
	}
	if ('<' != xml[startIndex]) {
		return -1;
	}
	if ('/' != xml[startIndex + 1]) {
		return -1;
	}

	bool isInDoubleQuote = false; // 是否在双括号中。
	bool isInSingleQuote = false; // 是否在单括号中。

	for (int i = startIndex + 2; i < xmlSize; i++) {
		char ch = xml[i];
		char next;
		if (i < xmlSize - 1) {
			next = xml[i + 1];
		}
		else {
			next = ' ';
		}
		// 开启双引号
		if (!isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = true;
		}
		// 开启单引号
		else if (!isInDoubleQuote && !isInSingleQuote && '\'' == ch) {
			bool isInSingleQuote = true;
		}
		// 双引号中避免反斜杠转义符的干扰
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\"' == next) {
			i++;
		}
		// 单引号中避免反斜杠转义符的干扰
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\'' == next) {
			i++;
		}
		// 关闭双引号
		else if (isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = false;
		}
		// 关闭单引号
		else if (!isInDoubleQuote && isInSingleQuote && '\'' == ch) {
			isInSingleQuote = false;
		}
		// 单标签
		else if (!isInDoubleQuote && !isInSingleQuote && '/' == ch && '>' == next) {
			return -1;
		}
		else if (!isInDoubleQuote && !isInSingleQuote && '>' == ch) {
			return i;
		}
	}

	return -1;
}






int Lexer::endIndex_SingleTagToken(char* xml, int xmlSize, int startIndex) {
	int noScanSize = xmlSize - 1 - startIndex;
	if (noScanSize < 4) {
		return -1;
	}
	if ('<' != xml[startIndex]) {
		return -1;
	}
	// 结束标签
	if ('<' == xml[startIndex] && '/' == xml[startIndex + 1]) {
		return -1;
	}

	bool isInDoubleQuote = false; // 是否在双括号中。
	bool isInSingleQuote = false; // 是否在单括号中。

	for (int i = startIndex + 2; i < xmlSize - 1; i++) {
		char ch = xml[i];
		char next;
		if (i < xmlSize - 1) {
			next = xml[i + 1];
		}
		else {
			next = ' ';
		}
		// 开启双引号
		if (!isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = true;
		}
		// 开启单引号
		else if (!isInDoubleQuote && !isInSingleQuote && '\'' == ch) {
			bool isInSingleQuote = true;
		}
		// 双引号中避免反斜杠转义符的干扰
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (isInDoubleQuote && !isInSingleQuote && '\\' == ch && '\"' == next) {
			i++;
		}
		// 单引号中避免反斜杠转义符的干扰
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\\' == next) {
			i++;
		}
		else if (!isInDoubleQuote && isInSingleQuote && '\\' == ch && '\'' == next) {
			i++;
		}
		// 关闭双引号
		else if (isInDoubleQuote && !isInSingleQuote && '\"' == ch) {
			isInDoubleQuote = false;
		}
		// 关闭单引号
		else if (!isInDoubleQuote && isInSingleQuote && '\'' == ch) {
			isInSingleQuote = false;
		}
		// 单标签
		else if (!isInDoubleQuote && !isInSingleQuote && '/' == ch && '>' == next) {
			return i + 1;
		}
	}

	return -1;
}


bool Lexer::isWhitespace(char ch) {
	bool result = false;
	if (' ' == ch || '\r' == ch || '\n' == ch || '\t' == ch) {
		result = true;
	}
	return result;
}


int Lexer::calTokenListLength(char* xml, int xmlSize) {
	bool preIsText = false; // 上一个Token是不是文本
	int tokenArrSize = 0;
	for (int i = 0; i < xmlSize;) {
		int endIndex = -1;
		if ((endIndex = Lexer::endIndex_XmlFileToken(xml, xmlSize, i)) > 0) {
			preIsText = false;
			tokenArrSize++;
			i = 1 + endIndex;
		}
		else if ((endIndex = Lexer::endIndex_BeginTagToken(xml, xmlSize, i)) > 0) {
			preIsText = false;
			tokenArrSize++;
			i = 1 + endIndex;
		}
		else if ((endIndex = Lexer::endIndex_EndTagToken(xml, xmlSize, i)) > 0) {
			preIsText = false;
			tokenArrSize++;
			i = 1 + endIndex;
		}
		else if ((endIndex = Lexer::endIndex_SingleTagToken(xml, xmlSize, i)) > 0) {
			preIsText = false;
			tokenArrSize++;
			i = 1 + endIndex;
		}
		else {
			// 上一个也是文本，把当前字符加入到上一个文本单词中。
			if (tokenArrSize > 0 && preIsText) {
			}
			// 上一个单词不是文本，或者是第一个字符。
			else {
				// 如果是空白字符串，没必要作为 TextToken 中的第一个字符,也就没必要创建一个新的文本单词。
				// 这样可以有效踢出全是空白字符的 TextToken.
				char ch = xml[i];
				if (!isWhitespace(ch)) {
					preIsText = true;
					tokenArrSize++;
				}
			}
			i++;
		}
	}
	return tokenArrSize;
}

void Lexer::generateTokenList(char* xml, int xmlSize, Token** outTokenArr, int* outTokenArrSize) {
	unique_ptr<StringBuilder> sb(new StringBuilder());
	unique_ptr<Logger> logger(new Logger());
	int tokenArrMax = Lexer::calTokenListLength(xml, xmlSize);
	(*outTokenArrSize) = tokenArrMax;
	if (tokenArrMax > 0) {
		(*outTokenArr) = new Token[tokenArrMax];

		int tokenArrSize = 0;
		for (int i = 0; i < xmlSize;) {
			//if () {

			//}

			int endIndex = -1;
			if ((endIndex = Lexer::endIndex_XmlFileToken(xml, xmlSize, i)) > 0) {
				if (tokenArrSize >= tokenArrMax) {
					sb->clear()->append("ERROR:  tokenArrSize=")->append(tokenArrSize)->append("   tokenArrMax=")->append(tokenArrMax);
					logger->info(sb.get());
					(* outTokenArrSize) = NULL;
					(*outTokenArrSize) = 0;
					break;
				}

				(*outTokenArr)[tokenArrSize].beginIndex = i;
				(*outTokenArr)[tokenArrSize].endIndex = endIndex;
				(*outTokenArr)[tokenArrSize].tagNameId = TagNameIdConst::XML_FILE_TAG;
				(*outTokenArr)[tokenArrSize].tokenType = TokenTypeConst::XmlFileToken;
				tokenArrSize++;
				i = 1 + endIndex;
			}
			else if ((endIndex = Lexer::endIndex_BeginTagToken(xml, xmlSize, i)) > 0) {
				if (tokenArrSize >= tokenArrMax) {
					sb->clear()->append("ERROR:  tokenArrSize=")->append(tokenArrSize)->append("   tokenArrMax=")->append(tokenArrMax);
					logger->info(sb.get());
					(*outTokenArrSize) = NULL;
					(*outTokenArrSize) = 0;
					break;
				}

				(*outTokenArr)[tokenArrSize].beginIndex = i;
				(*outTokenArr)[tokenArrSize].endIndex = endIndex;
				(*outTokenArr)[tokenArrSize].tagNameId = Lexer::getTagNameId(xml, i, endIndex);
				(*outTokenArr)[tokenArrSize].tokenType = TokenTypeConst::BeginTagToken;
				tokenArrSize++;
				i = 1 + endIndex;
			}
			else if ((endIndex = Lexer::endIndex_EndTagToken(xml, xmlSize, i)) > 0) {
				if (tokenArrSize >= tokenArrMax) {
					sb->clear()->append("ERROR:  tokenArrSize=")->append(tokenArrSize)->append("   tokenArrMax=")->append(tokenArrMax);
					logger->info(sb.get());
					(*outTokenArrSize) = NULL;
					(*outTokenArrSize) = 0;
					break;
				}

				(*outTokenArr)[tokenArrSize].beginIndex = i;
				(*outTokenArr)[tokenArrSize].endIndex = endIndex;
				(*outTokenArr)[tokenArrSize].tagNameId = Lexer::getTagNameId(xml, i, endIndex);
				(*outTokenArr)[tokenArrSize].tokenType = TokenTypeConst::EndTagToken;
				tokenArrSize++;
				i = 1 + endIndex;
			}
			else if ((endIndex = Lexer::endIndex_SingleTagToken(xml, xmlSize, i)) > 0) {
				if (tokenArrSize >= tokenArrMax) {
					sb->clear()->append("ERROR:  tokenArrSize=")->append(tokenArrSize)->append("   tokenArrMax=")->append(tokenArrMax);
					logger->info(sb.get());
					(*outTokenArrSize) = NULL;
					(*outTokenArrSize) = 0;
					break;
				}

				(*outTokenArr)[tokenArrSize].beginIndex = i;
				(*outTokenArr)[tokenArrSize].endIndex = endIndex;
				(*outTokenArr)[tokenArrSize].tagNameId = Lexer::getTagNameId(xml, i, endIndex);
				(*outTokenArr)[tokenArrSize].tokenType = TokenTypeConst::SingleTagToken;
				tokenArrSize++;
				i = 1 + endIndex;
			}
			else {
				// 上一个也是文本，把当前字符加入到上一个文本单词中。
				if (tokenArrSize > 0 && TokenTypeConst::TextToken == (*outTokenArr)[tokenArrSize - 1].tokenType) {
					(*outTokenArr)[tokenArrSize - 1].endIndex = i;
				}
				// 上一个单词不是文本，或者是第一个字符。
				else {
					// 如果是空白字符串，没必要作为 TextToken 中的第一个字符,也就没必要创建一个新的文本单词。
					// 这样可以有效踢出全是空白字符的 TextToken.
					char ch = xml[i];
					if (!isWhitespace(ch)) {
						if (tokenArrSize >= tokenArrMax) {
							sb->clear()->append("ERROR:  tokenArrSize=")->append(tokenArrSize)->append("   tokenArrMax=")->append(tokenArrMax);
							logger->info(sb.get());
							(*outTokenArrSize) = NULL;
							(*outTokenArrSize) = 0;
							break;
						}

						(*outTokenArr)[tokenArrSize].beginIndex = i;
						(*outTokenArr)[tokenArrSize].endIndex = i;
						(*outTokenArr)[tokenArrSize].tokenType = TokenTypeConst::TextToken;
						tokenArrSize++;
					}
				}
				i++;
			}
		} // end for (int i = 0; i < xmlSize;)
	}
}


