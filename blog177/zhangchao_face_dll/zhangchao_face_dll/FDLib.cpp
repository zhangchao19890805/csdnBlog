#include "pch.h"
#include "FDLib.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "Token.h"
#include "Lexer.h"
#include "TokenTypeConst.h"
#include "TagNameIdConst.h"
#include "Logger.h"
#include "StringBuilder.h"

using namespace std;

bool FDLib::listAll(int lUserID, HkXmlResult* hkXmlResult) {
	bool result = false;
	char requestUrlArr[64];
	sprintf_s(requestUrlArr, "GET /ISAPI/Intelligent/FDLib");
	NET_DVR_XML_CONFIG_INPUT inputParam = { 0 };
	inputParam.dwSize = sizeof(inputParam);
	inputParam.lpRequestUrl = requestUrlArr;
	inputParam.dwRequestUrlLen = (unsigned long)strlen(requestUrlArr);

	NET_DVR_XML_CONFIG_OUTPUT outputParam = { 0 };
	outputParam.dwSize = sizeof(outputParam);
	outputParam.lpOutBuffer = hkXmlResult->xml;
	outputParam.dwOutBufferSize = hkXmlResult->xmlArrLength;

	BOOL flag = NET_DVR_STDXMLConfig(lUserID, &inputParam, &outputParam);

	if (flag) {
		hkXmlResult->xmlSize = outputParam.dwReturnedXMLSize;
		result = true;
	}
	return result;
}


char* FDLib::getFDID(HkXmlResult* hkXmlResult) {
	char* fdid = NULL;
	Token* tokenArr = NULL;
	int tokenArrSize = 0;
	Lexer::generateTokenList(hkXmlResult->xml, hkXmlResult->xmlSize, &tokenArr, &tokenArrSize);

	if (NULL != tokenArr) {
		int beginIndex = -1;
		int endIndex = -1;
		int fdidTokenIndex = -1;
		for (int i = 0; i < tokenArrSize; i++) {
			Token token = tokenArr[i];
			if (TagNameIdConst::FDID == token.tagNameId    && TokenTypeConst::BeginTagToken == token.tokenType) {
				fdidTokenIndex = i + 1;
			}
		}
		if (fdidTokenIndex > -1) {
			beginIndex = tokenArr[fdidTokenIndex].beginIndex;
			endIndex = tokenArr[fdidTokenIndex].endIndex;
		}
		if (beginIndex >= 0 && endIndex >= 0 && beginIndex <= endIndex) {
			int fdidMax = endIndex - beginIndex + 2;
			fdid = new char[fdidMax];
			for (int i = beginIndex; i <= endIndex; i++) {
				int fdidIndex = i - beginIndex;
				fdid[fdidIndex] = hkXmlResult->xml[i];
			}
			fdid[fdidMax - 1] = '\0';
		}
	
		delete[] tokenArr;
	}
	return fdid;
}



char* FDLib::add(int lUserID) {
	unique_ptr<StringBuilder> sb(new StringBuilder());
	// 日志
	unique_ptr<Logger> logger(new Logger());

	// 增加人脸库后，新的人脸库的ID
	char* fdid = NULL;

	//组装输入参数的XML
	char* lpInBuffer = new char[10 * 1024];
	sprintf_s(lpInBuffer, 10 * 1024, "%s%s%s%s%s%s%s%s%s",
		"<CreateFDLibList version=\"2.0\" xmlns=\"http://www.isapi.org/ver20/XMLSchema\">",
			"<CreateFDLib>",
				"<id>1</id>",
				"<name>1</name>",
				"<thresholdValue>50</thresholdValue>",
				"<customInfo></customInfo>",
				"<customFaceLibID></customFaceLibID>",
			"</CreateFDLib>",
		"</CreateFDLibList>");


	char requestUrlArr[64];
	sprintf_s(requestUrlArr, "POST /ISAPI/Intelligent/FDLib");
	NET_DVR_XML_CONFIG_INPUT inputParam = { 0 };
	inputParam.dwSize = sizeof(inputParam);
	inputParam.lpRequestUrl = requestUrlArr;
	inputParam.dwRequestUrlLen = (unsigned long)strlen(requestUrlArr);
	inputParam.lpInBuffer = lpInBuffer;
	inputParam.dwInBufferSize = 1 + (unsigned long)strlen(lpInBuffer);


	NET_DVR_XML_CONFIG_OUTPUT outputParam = { 0 };
	outputParam.dwSize = sizeof(outputParam);
	outputParam.dwOutBufferSize = 1024 * 1024;
	// char* addReturnXml = new char[outputParam.dwOutBufferSize];
	unique_ptr<char[]> addReturnXml(new char[outputParam.dwOutBufferSize]);
	outputParam.lpOutBuffer = addReturnXml.get();
	
	// 执行XML透传函数。
	BOOL flag = NET_DVR_STDXMLConfig(lUserID, &inputParam, &outputParam);

	// 如果创建成功，就查询新建的人脸库的ID。
	if (flag) {
		sb->clear()->append("addReturnXml=")->append(addReturnXml.get());
		logger->info(sb.get());


		// 获取人脸库列表的XML结果。
		unique_ptr<HkXmlResult> fdlibResult(new HkXmlResult());
		bool fdlibFlag = FDLib::listAll(lUserID, fdlibResult.get());
		if (fdlibFlag) {
			// 人脸库的ID。
			fdid = FDLib::getFDID(fdlibResult.get());
			if (NULL == fdid) {
				logger->info("After add fdlib, fdid is null.");
			}
			else {
				sb->clear()->append("After add fdlib, fdid =")->append(fdid);
				logger->info(sb.get());
			}
		}
	}

	

	// 释放资源。
	if (NULL != lpInBuffer) {
		delete[] lpInBuffer;
		lpInBuffer = NULL;
	}
	//if (NULL != addReturnXml) {
	//	delete[] addReturnXml;
	//}
	return fdid;
}

