﻿#include "pch.h"
#include "jni.h"
#include <Windows.h>
#include "HCNetSDK.h"

#include <iostream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include "FDLibCapabilities.h"
#include "com_rzfk_jni_FaceJni.h"
#include "HkXmlResult.h"
#include "FDLib.h"
#include "FDSearch.h"
#include "DelPicture.h"
#include "UploadPicture.h"
#include "FileUtils.h"
#include "StringBuilder.h"
#include "Logger.h"

using namespace std;


// 设备登录
int DoLogin(const char* ip)
{
	printf("ip=%s\n", ip);
	unique_ptr<StringBuilder> sb(new StringBuilder);
	unique_ptr<Logger> logger(new Logger());
	sb->clear()->append("ip=")->append(ip);
	logger->info(sb.get());
	NET_DVR_USER_LOGIN_INFO struLoginInfo = { 0 };
	struLoginInfo.bUseAsynLogin = 0; //同步登录方式
	strcpy_s(struLoginInfo.sDeviceAddress, ip); //设备IP地址
	struLoginInfo.wPort = 8000; //设备服务端口
	strcpy_s(struLoginInfo.sUserName, "your_user_name"); //设备登录用户名
	strcpy_s(struLoginInfo.sPassword, "your_password"); //设备登录密码
	//设备参数结构体
	NET_DVR_DEVICEINFO_V40 DeviceInfoTmp;
	memset(&DeviceInfoTmp, 0, sizeof(NET_DVR_DEVICEINFO_V40));
	// 向设备注册
	int lLoginID = NET_DVR_Login_V40(&struLoginInfo, &DeviceInfoTmp);
	if (lLoginID < 0)
	{
		cout << "注册失败, 错误码: " << NET_DVR_GetLastError() << endl;
		logger->info(sb->clear()->appendGbkToUtf8("注册失败, 错误码: ")->append(NET_DVR_GetLastError()));
		NET_DVR_Cleanup();
	}
	if (lLoginID >= 0)
	{
		cout << "注册成功" << endl;
		logger->info(sb->clear()->appendGbkToUtf8("注册成功"));
	}
	return lLoginID;
}

JNIEXPORT jint JNICALL Java_com_rzfk_jni_FaceJni_updateFaceLib
(JNIEnv* env, jobject obj, jstring ip) {
	
	const char* ipChPtr = env->GetStringUTFChars(ip, NULL);

	// 日志
	unique_ptr<Logger> logger(new Logger());

	NET_DVR_Init();
	// 开启海康日志
	char arr[] = "D:\\your_hk_log_folder\\";
	char* logFileDir = arr;
	NET_DVR_SetLogToFile(3, logFileDir, FALSE);
	//设置连接时间与重连时间
	NET_DVR_SetConnectTime(2000, 1);
	NET_DVR_SetReconnect(10000, true);

	int lUserID = DoLogin(ipChPtr);
	if (lUserID < 0)
	{
		// 清理IP占用的资源。
		env->ReleaseStringUTFChars(ip, ipChPtr);
		return -1;
	}
	jint result = 1;

	unique_ptr<StringBuilder> sb(new StringBuilder);
	sb->clear()->append("lUserID=")->append(lUserID);
	logger->info(sb.get());

	// 人脸库能力集的XML结果。
	unique_ptr<HkXmlResult> capabilitiesResult(new HkXmlResult());
	// 获取人脸库列表的XML结果。
	unique_ptr<HkXmlResult> fdlibResult(new HkXmlResult());
	

	char* fdid = NULL;


	// 判断人脸搜索分页是从0开始还是从1开始。
	bool capabilitiesFlag = FDLibCapabilities::getCapabilities(lUserID, capabilitiesResult.get());
	bool fdlibSearchStart = 1;
	if (capabilitiesFlag) {
		bool isSupportStandardSearch = FDLibCapabilities::getIsSupportStandardSearch(capabilitiesResult.get());
		if (isSupportStandardSearch) {
			fdlibSearchStart = 0;
		}
	}
	sb->clear()->append("fdlibSearchStart=")->append(fdlibSearchStart);
	logger->info(sb.get());

	// 获取人脸库的XML结果。
	bool fdlibFlag = FDLib::listAll(lUserID, fdlibResult.get());
	if (fdlibFlag) {
		sb->clear()->append("fdid xml=")->append(fdlibResult->xml);
		logger->info(sb.get());
		// logger->outfile << endl << endl << endl << "fdid xml=" << fdlibResult->xml << endl;
		// 人脸库的ID。
		fdid = FDLib::getFDID(fdlibResult.get());
		if (NULL == fdid) {
			fdid = FDLib::add(lUserID);
		}

		if (NULL != fdid) {
			int fdidLength = int(strlen(fdid));
			//logger->outfile << "fdid=" << fdid << "    length=" << strlen(fdid) << endl;
			sb->clear()->append("fdid=")->append(fdid)->append("    length=")->append(int(strlen(fdid)));
			logger->info(sb.get());

			// 获取人脸搜索分页的总条数的结果。
			unique_ptr<HkXmlResult> fdSearchResultTotal(new HkXmlResult());
			bool fdSearchFlagTotal = FDSearch::search(lUserID, fdlibSearchStart, fdid, fdSearchResultTotal.get());
			int fdSearchPageTotal = FDSearch::getPageTotal(fdSearchResultTotal.get());
			for (int fdSearchIndex = 0; fdSearchFlagTotal && fdSearchIndex < fdSearchPageTotal; fdSearchIndex++) {
				// 获取人脸搜索分页的XML结果。
				unique_ptr<HkXmlResult> fdSearchResult(new HkXmlResult());
			
				bool fdSearchFlag = FDSearch::search(lUserID, fdlibSearchStart, fdid, fdSearchResult.get());
				if (fdSearchFlag) {
					sb->clear()->append("fdSearchResult->xmlSize=")->append(fdSearchResult->xmlSize)->append("\n")
						->append("xml=\n")->append(fdSearchResult->xml);
					logger->info(sb.get());

					// PID 列表。
					char** pictureIdList = NULL;
					int pictureIdListLength = 0;
				
					FDSearch::getPictureIdList(fdSearchResult.get(), &pictureIdList, &pictureIdListLength);
					for (int i = 0; NULL != pictureIdList && i < pictureIdListLength; i++) {
						if (NULL != pictureIdList[i]) {
							//logger->outfile << "pictureIdList[" << i << "]=" << pictureIdList[i] << endl;
							sb->clear()->append("pictureIdList[")->append(i)->append("]=")->append(pictureIdList[i]);
							logger->info(sb.get());
							DelPicture::deleteById(lUserID, fdid, pictureIdList[i]);
						}
					}

					// 回收PID列表。
					for (int i = 0; NULL != pictureIdList && i < pictureIdListLength; i++) {
						if (NULL != pictureIdList[i]) {
							delete[] pictureIdList[i];
							pictureIdList[i] = NULL;
						}
					}
					delete[] pictureIdList;
				}
			} // end for


			// 上传头像图片
			// 读取所有文件.
			char*** faceList = NULL;
			int faceListLength = 0;
			int readFaceFlag = FileUtils::readXmlPic(&faceList, &faceListLength);
			if (1 == readFaceFlag) {
				for (int i = 0; i < faceListLength; i++) {
					UploadPicture::SetUserFace(lUserID, 1, fdid, faceList[i][0], faceList[i][1]);
				}
			}
			else if (0 == readFaceFlag) {
				logger->info("NULL == xmlList || NULL == picList");
			}
			else if (-1 == readFaceFlag) {
				logger->info("写入outFaceList时缓冲区溢出");
			}

			
			if (NULL != faceList) {
				for (int indexLv1 = 0; indexLv1 < faceListLength; indexLv1++) {

					if (NULL != faceList[indexLv1]) {
						for (int indexLv2 = 0; indexLv2 < 2; indexLv2++) {
							if (NULL != faceList[indexLv1][indexLv2]) {
								delete[] faceList[indexLv1][indexLv2];
								faceList[indexLv1][indexLv2] = NULL;
							}
						}
						delete[] faceList[indexLv1];
						faceList[indexLv1] = NULL;
					}
				}
				delete[] faceList;
			}
			else {
				if (0 == readFaceFlag) {
					result = -4;
				}
				else if (-1 == readFaceFlag) {
					result = -5;
				}
				else {
					result = -6;
				}
			}
		} // end  if (NULL != fdid)
		else {
			result = -3;
		}
	}
	else {
		result = -2;
	}
	

	
//	********* BEGIN 清理资源 *********** 
	//注销用户
	NET_DVR_Logout(lUserID);
	//释放SDK资源
	NET_DVR_Cleanup();
	// 清理IP占用的资源。
	env->ReleaseStringUTFChars(ip, ipChPtr);
	// 删除字符串
	if (NULL != fdid) {
		delete[] fdid;
	}
//	********* END 清理资源 ***********
	 
	return result;
}


