#pragma once
class FileUtils
{
private:
	static int getChildrenLength(char* inDirPath);

	static int getXmlPicLength(char** xmlList, int xmlListLength, char** picList, int picListLength);

	static int findIndex(char** picList, const int picListLength, const char* str);

public:
	static void readDir(char* inDirPath, char*** outFileList, int* outFileListLength);

	static int readXmlPic(char**** outFaceList, int* outFaceListLength);

	static char* getFileMainName(char* fileName);
};

