#include "pch.h"
#include "StringBuilder.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "StringUtils.h"

using namespace std;

StringBuilder::StringBuilder(int capacity) {
	this->capacity = capacity;
	this->value = new char[this->capacity];
	this->count = 0;
}

StringBuilder::StringBuilder() {
	this->capacity = 128;
	this->value = new char[this->capacity];
	this->count = 0;
}


StringBuilder::~StringBuilder() {
	this->capacity = 0;
	this->count = 0;
	delete[] this->value;
}

void StringBuilder::ensureCapacity(int minimumCapacity) {
	this->capacity = 2 * minimumCapacity + 2;
	char* oldValue = this->value;
	this->value = new char[this->capacity];
	for (int i = 0; i < count; i++) {
		if (i < this->capacity) {
			this->value[i] = oldValue[i];
		}
	}
	delete[] oldValue;
	oldValue = NULL;
}

StringBuilder* StringBuilder::append(char* str, int strLength) {
	if (NULL == str) {
		if (this->capacity <= count + 4) {
			ensureCapacity(count + 4);
		}
		this->value[count + 1] = 'N';
		this->value[count + 2] = 'U';
		this->value[count + 3] = 'L';
		this->value[count + 4] = 'L';
		this->count += 4;
	}
	else {
		if (this->capacity <= count + strLength) {
			ensureCapacity(count + strLength);
		}
		for (int i = 0; i < strLength; i++) {
			int index = this->count + i;
			this->value[index] = str[i];
		}
		this->count += strLength;
	}
	return this;
}

StringBuilder* StringBuilder::append(char* str) {
	return this->append(str, int(strlen(str)));
}


StringBuilder* StringBuilder::append(const char* str) {
	char* tmp = _strdup(str);
	return this->append(tmp, int(strlen(str)));
}

StringBuilder* StringBuilder::append(int n) {
	char tmp[32];
	sprintf_s(tmp, 32, "%d", n);
	return this->append(tmp, int(strlen(tmp)));
}

StringBuilder* StringBuilder::append(long long n) {
	char tmp[32];
	sprintf_s(tmp, 32, "%lld", n);
	return this->append(tmp, int(strlen(tmp)));
}

StringBuilder* StringBuilder::append(unsigned long n) {
	char tmp[32];
	sprintf_s(tmp, 32, "%d", n);
	return this->append(tmp, int(strlen(tmp)));
}

StringBuilder* StringBuilder::append(long n) {
	char tmp[32];
	sprintf_s(tmp, 32, "%d", n);
	return this->append(tmp, int(strlen(tmp)));
}

StringBuilder* StringBuilder::appendGbkToUtf8(char* str) {
	char* utf8Str = StringUtils::gbkToUtf8(str);
	this->append(utf8Str, int(strlen(utf8Str)));
	delete[] utf8Str;
	return this;
}

StringBuilder* StringBuilder::appendGbkToUtf8(const char* str) {
	char* utf8Str = StringUtils::gbkToUtf8(str);
	this->append(utf8Str, int(strlen(utf8Str)));
	delete[] utf8Str;
	return this;
}

char* StringBuilder::toString() {
	int arrLength = this->count + 1;
	char* result = new char[arrLength];
	for (int i = 0; i < this->count; i++) {
		if (i < arrLength) {  // ���⾯��
			result[i] = this->value[i];
		}
	}
	result[arrLength - 1] = '\0';
	return result;
}

StringBuilder* StringBuilder::clear() {
	this->count = 0;
	return this;
}