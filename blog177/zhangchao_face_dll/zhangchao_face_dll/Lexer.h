#pragma once
#include "Token.h"

class Lexer
{
private:
	static bool startWith(char* xml, int startIndex, int endIndex, char tagName[]);

	// 识别出XML文件标签，返回结束字符的位置。
	static int endIndex_XmlFileToken(char* xml, int xmlSize, int startIndex);

	// 识别出开始标签，返回结束字符的位置。
	static int endIndex_BeginTagToken(char* xml, int xmlSize, int startIndex);

	// 识别出结束标签，返回结束字符的位置。
	static int endIndex_EndTagToken(char* xml, int xmlSize, int startIndex);

	// 识别出单标签，返回结束字符的位置。
	static int endIndex_SingleTagToken(char* xml, int xmlSize, int startIndex);

	// 计算Token数组的长度
	static int calTokenListLength(char* xml, int xmlSize);

public:
	// 获得XML标签名称ID
	static int getTagNameId(char* xml, int startIndex, int endIndex);

	// 判断是不是空白字符串
	static bool isWhitespace(char ch);

	// 生成Token数组
	static void generateTokenList(char* xml, int xmlSize, Token** outTokenArr, int* outTokenArrSize);
};

