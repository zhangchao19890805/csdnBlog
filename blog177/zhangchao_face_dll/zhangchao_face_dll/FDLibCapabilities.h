#pragma once
#include "HkXmlResult.h"
class FDLibCapabilities
{
public:
	// 返回值表示接口是否执行成功
	static bool getCapabilities(int lUserID, HkXmlResult* hkXmlResult);

	static bool getIsSupportStandardSearch(HkXmlResult* hkXmlResult);
};

