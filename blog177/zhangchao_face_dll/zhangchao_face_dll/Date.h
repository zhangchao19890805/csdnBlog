#pragma once
class Date
{
private:
	int year;
	int month;
	int day;
	static bool checkDate(int year, int month, int day);

public:
	Date(bool isNow);
	~Date();

	bool setDate(int year, int month, int day);

	int getYear();
	int getMonth();
	int getDay();

	// 考虑到返回新的Date，有可能产生指针管理方面的麻烦，所以这里直接操作自己的成员变量。
	void minusDay(int days);
	void addDay(int days);

	// 比较两个日期
	// -1 d1 < d2
	// 0  d1 == d2
	//  1 d1 > d2
	// -100  d1或者d2参数有问题
	static int compare(Date* d1, Date* d2);
};

