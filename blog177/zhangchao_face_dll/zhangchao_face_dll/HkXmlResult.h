#pragma once
class HkXmlResult
{
public:
	// xml实际大小
	unsigned long xmlSize;
	// xml指针,指向char 数组
	char* xml;
	// xml的数组长度
	unsigned long xmlArrLength;

	HkXmlResult();
	~HkXmlResult();	

};

