package zhangchao;

/**
 * 传递进程信息的类
 * @author zhangchao
 */
public class ProcessDto {
    // 进程id
    private Integer pid;

    // 进程名称
    private String name;

    // 进程开始时间
    private String startDateStr;

    // 命令行参数
    private String cmdline;

    ///////////////////////////////
    //  setters/getters
    //////////////////////////////


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ProcessDto{");
        sb.append("pid=").append(pid);
        sb.append(", name='").append(name).append('\'');
        sb.append(", startDateStr='").append(startDateStr).append('\'');
        sb.append(", cmdline='").append(cmdline).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public String getCmdline() {
        return cmdline;
    }

    public void setCmdline(String cmdline) {
        this.cmdline = cmdline;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }
}
