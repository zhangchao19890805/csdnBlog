package zhangchao;

import java.util.ArrayList;
import java.util.List;

/**
 * 调用JNI的类
 *
 * cd 进入D:\ws\csdnBlog\blog185\blog185java\target\classes>
 * javah -jni zhangchao.ZcProcessJni  获取 zhangchao_ZcProcessJni.h 文件
 *
 * javap -s -p zhangchao.ZcProcessJni 获取 Java 方法的信息
 *
 * Compiled from "ZcProcessJni.java"
 * public class zhangchao.ZcProcessJni {
 *   public static java.lang.String ERROR_LOAD_MESSAGE;
 *     descriptor: Ljava/lang/String;
 *   public java.util.List<zhangchao.ProcessDto> processList;
 *     descriptor: Ljava/util/List;
 *   public zhangchao.ZcProcessJni();
 *     descriptor: ()V
 *
 *   public native void lookProcess();
 *     descriptor: ()V
 *
 *   public native void killProcess(int);
 *     descriptor: (I)V
 *
 *   private void callback(int, java.lang.String, java.lang.String, java.lang.String);
 *     descriptor: (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 *
 *   static {};
 *     descriptor: ()V
 * }
 *
 *
 *
 * @author zhangchao
 *
 */
public class ZcProcessJni {
    public static String ERROR_LOAD_MESSAGE = null;

    // 存放若干进程信息
    public List<ProcessDto> processList = new ArrayList<>();

    /**
     * 查找所有进程信息
     */
    public native void lookProcess();

    /**
     * 杀死进程ID指定的进程
     * @param pid 进程ID
     */
    public native void killProcess(int pid);

    /**
     * C++ 调用 Java 的回调方法。用于返回查找到的进程信息。
     * @param pid  进程ID
     * @param name　　进程名称
     * @param startDateStr　进程启动时间
     * @param cmdline　　进程的命令行命令
     */
    private void callback(int pid, String name, String startDateStr, String cmdline) {
        ProcessDto dto = new ProcessDto();
        dto.setPid(pid);
        dto.setName(name);
        dto.setStartDateStr(startDateStr);
        dto.setCmdline(cmdline);
        processList.add(dto);
    }

    static {
        try {
            // 加载dll文件
            System.loadLibrary("blog185vc");
        } catch (Throwable e) {
            StringBuilder sb = new StringBuilder();
            StackTraceElement[] stackTraceElements = e.getStackTrace();
            if (null != stackTraceElements) {
                for (StackTraceElement stackTraceElement : stackTraceElements) {
                    sb.append(stackTraceElement).append("\n");
                }
            }
            ERROR_LOAD_MESSAGE = sb.toString();
        }

    }

}
