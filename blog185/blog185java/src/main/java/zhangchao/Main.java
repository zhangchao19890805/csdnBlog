package zhangchao;

public class Main {
    public static void main(String[] args) {
        System.out.println("begin");
        if (ZcProcessJni.ERROR_LOAD_MESSAGE != null &&
                ZcProcessJni.ERROR_LOAD_MESSAGE.length() > 0) {
            System.out.println("Error: " + ZcProcessJni.ERROR_LOAD_MESSAGE);
            return;
        }
        ZcProcessJni zcProcessJni = new ZcProcessJni();
        zcProcessJni.lookProcess();
        if (null != zcProcessJni.processList && !zcProcessJni.processList.isEmpty()) {
            for (ProcessDto item : zcProcessJni.processList) {
                // 查找到视频播放软件VLC的进程，就打印出进程信息，并且杀死进程。
                if ("vlc.exe".equalsIgnoreCase(item.getName())) {
                    System.out.println(item);
                    zcProcessJni.killProcess(item.getPid());
                }
            }
        }
        System.out.println("end");
    }
}
