#pragma once
class StringBuilder
{

private:
	char* value;  // 保存字符的数组指针，不含 \0 结束字符
	int capacity; // value 字符数组的长度
	int count; // 字符个数

	void ensureCapacity(int minimumCapacity);
	char* gbkToUtf8(const char* src_str);

public:
	StringBuilder(int capacity);
	StringBuilder();
	~StringBuilder();

	StringBuilder* append(char* str, int strLength);
	StringBuilder* append(char* str);
	StringBuilder* append(const char* str);
	StringBuilder* append(int n);
	StringBuilder* append(long long n);
	StringBuilder* append(unsigned long n);
	StringBuilder* append(long n);
	StringBuilder* appendGbkToUtf8(char* str);
	StringBuilder* appendGbkToUtf8(const char* str);


	// 创建一个新的char* 类型字符串，并且返回。
	char* toString();

	// 清空。
	StringBuilder* clear();
};

