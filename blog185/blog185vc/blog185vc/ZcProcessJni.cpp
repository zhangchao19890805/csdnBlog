﻿#include <stdio.h>
#include<string.h>
#include <fstream>
#include "pch.h"
#include "jni.h"
#include "zhangchao_ZcProcessJni.h"
#include <windows.h>
#include "errhandlingapi.h"
#include <tlhelp32.h>	//进程快照函数头文件
#include <stringapiset.h>
#include<tchar.h>
#include<Winternl.h>
#include "StringBuilder.h"
#include "Logger.h"

using namespace std;


typedef NTSTATUS(WINAPI* QT)(HANDLE, PROCESSINFOCLASS, PVOID, ULONG, PULONG);//NtQueryInformationProcess

/*
写成一个函数，来获得所有的PEB结构体信息
*/
char* GetProcessCommandLine(HANDLE hProcess)
{
	HMODULE hModule = 0;
	QT NtQuery = { 0 };
	hModule = LoadLibrary(L"Ntdll.dll");  //加载动态链接
	if (hModule)
	{
		NtQuery = (QT)GetProcAddress(hModule, "NtQueryInformationProcess");
		if (NtQuery == NULL)
			return 0;
	}
	else
		return 0;
	PROCESS_BASIC_INFORMATION pi = { 0 };
	NTSTATUS re = NtQuery(hProcess,
		ProcessBasicInformation, &pi, sizeof(pi), NULL);

	/*
	在pi中，pi.PebBaseAddress指向PEB，那么这个指针指向的地址是哪个地址呢？
	这个指针指向的地址，是word进程的地址
	*/
	if (!NT_SUCCESS(re))
	{
		//	_tprintf(L"OK\n");
		return 0;
	}

	PEB peb;
	RTL_USER_PROCESS_PARAMETERS para;

	ReadProcessMemory(hProcess, pi.PebBaseAddress, &peb, sizeof(peb), NULL); //将pi.PebBaseAddress的信息读取到peb中

	ReadProcessMemory(hProcess, peb.ProcessParameters, &para, sizeof(para), NULL);

	wchar_t* CommandLine = new wchar_t[1024];
	ReadProcessMemory(hProcess, para.CommandLine.Buffer, CommandLine, 1024 * 2, NULL);


	int iSize = WideCharToMultiByte(CP_ACP, 0, CommandLine, -1, NULL, 0, NULL, NULL);
	char* cmdline = new char[iSize + 10];
	WideCharToMultiByte(CP_ACP, 0, CommandLine, -1, cmdline, iSize, NULL, NULL);
	FreeLibrary(hModule);
	return cmdline;
}


/*
 * Class:     zhangchao_ZcProcessJni
 * Method:    lookProcess
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_zhangchao_ZcProcessJni_lookProcess(JNIEnv* env, jobject obj)
{
	// 获取Java对象中方法的ID
	jclass cls = env->GetObjectClass(obj);
	jmethodID mid = env->GetMethodID(cls, "callback", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
	if (NULL == mid) {
		return;
	}

	PROCESSENTRY32W currentProcess;						//存放快照进程信息的一个结构体
	currentProcess.dwSize = sizeof(currentProcess);		//在使用这个结构之前，先设置它的大小
	HANDLE hProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//给系统内的所有进程拍一个快照

	if (hProcess == INVALID_HANDLE_VALUE)
	{
		printf("CreateToolhelp32Snapshot()调用失败!\n");
		return;
	}

	bool bMore = Process32FirstW(hProcess, &currentProcess);	//获取第一个进程信息
	while (bMore)
	{
		HANDLE process = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, currentProcess.th32ProcessID);
		if (process != NULL) {
			FILETIME ftCreation, ftExit, ftKernel, ftUser;
			SYSTEMTIME stCreation, lstCreation;
			if (GetProcessTimes(process, &ftCreation, &ftExit, &ftKernel, &ftUser)) {
				FileTimeToSystemTime(&ftCreation, &stCreation);
				SystemTimeToTzSpecificLocalTime(NULL, &stCreation, &lstCreation);

				char nameArr[256];
				int iSize = WideCharToMultiByte(CP_ACP, 0, currentProcess.szExeFile, -1, NULL, 0, NULL, NULL);
				WideCharToMultiByte(CP_ACP, 0, currentProcess.szExeFile, -1, nameArr, iSize, NULL, NULL);
				jstring nameStr = env->NewStringUTF(nameArr);

				char timeArr[128];
				sprintf_s(timeArr, "%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d", lstCreation.wYear, lstCreation.wMonth, lstCreation.wDay,
					lstCreation.wHour, lstCreation.wMinute, lstCreation.wSecond);
				jstring timeStr = env->NewStringUTF(timeArr);

				char* cmdline = GetProcessCommandLine(process);
				jstring jsCmdline = env->NewStringUTF(cmdline);

				env->CallVoidMethod(obj, mid, currentProcess.th32ProcessID, nameStr, timeStr, jsCmdline);
			}
			CloseHandle(process);
		}
		else {
			//wcout << "PID=" << currentProcess.th32ProcessID << "   PName=" << currentProcess.szExeFile << endl;
		}


		bMore = Process32NextW(hProcess, &currentProcess);	//遍历下一个

	}
	CloseHandle(hProcess);	//清除hProcess句柄
}



/*
 * Class:     zhangchao_ZcProcessJni
 * Method:    killProcess
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_zhangchao_ZcProcessJni_killProcess(JNIEnv* env, jobject obj, jint pid) 
{
	unique_ptr<Logger> logger(new Logger());
	unique_ptr<StringBuilder> sb{ new StringBuilder() };
	logger->info("BEGIN killProcess");

	PROCESSENTRY32W currentProcess;						//存放快照进程信息的一个结构体
	currentProcess.dwSize = sizeof(currentProcess);		//在使用这个结构之前，先设置它的大小
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//给系统内的所有进程拍一个快照
	if (hSnapshot == INVALID_HANDLE_VALUE)
	{
		logger->info("CreateToolhelp32Snapshot()调用失败!");
		return;
	}

	bool bMore = Process32FirstW(hSnapshot, &currentProcess);	//获取第一个进程信息
	while (bMore) {
		//		HANDLE process = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, currentProcess.th32ProcessID);
				// 各种进程权限 https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-openprocess
		HANDLE process = OpenProcess(PROCESS_TERMINATE, FALSE, currentProcess.th32ProcessID);
		if (process != NULL) {
			if (pid == currentProcess.th32ProcessID) {

				int terminateFlag = TerminateProcess(process, 0);
				if (terminateFlag) {
					logger->info(sb->clear()->append("Success TerminateProcess(process, 0);   pid=")->append(pid));
				}
				else {
					logger->info(sb->clear()->append("Fail TerminateProcess(process, 0);  pid=")->append(pid));
				}

			}
			CloseHandle(process);
		}
		else {
			logger->info(sb->clear()->append("OpenProcess failed. process == NULL.  pid=")->
				append(pid)->append("  GetLastError()=")->append(GetLastError()));
		}
		bMore = Process32NextW(hSnapshot, &currentProcess);	//遍历下一个
	}

	CloseHandle(hSnapshot);
	logger->info("END killProcess");
}



