#include "pch.h"
#include "Date.h"
#include <time.h>


using namespace std;


Date::Date() {
	this->year = -1;
	this->month = -1;
	this->day = -1;
}

Date::Date(bool isNow) {
	if (isNow) {
		struct tm t;   //tm结构指针
		time_t now;  //声明time_t类型变量
		time(&now);      //获取系统日期和时间
		localtime_s(&t, &now);   //获取当地日期和时间

		this->year = t.tm_year + 1900;
		this->month = t.tm_mon + 1;
		this->day = t.tm_mday;
	}
	else {
		this->year = -1;
		this->month = -1;
		this->day = -1;
	}
}

Date::~Date() {
}


bool Date::checkDate(int year, int month, int day) {
	if (year <= 0 || day < 1 || day > 31) {
		return false;
	}
	if ((4 == month || 6 == month || 9 == month || 11 == month) && day <= 30) {
		return true;
	}
	if ((1 == month || 3 == month || 5 == month || 7 == month || 8 == month || 10 == month || 12 == month) && day <= 31) {
		return true;
	}
	if (2 == month) {
		int maxFebruary = 28;
		if ((0 == year % 100) && (0 == year % 400) && month <= 29) {
			maxFebruary = 29;
		}
		else if ((0 != year % 100) && (0 == year % 4) && month <= 29) {
			maxFebruary = 29;
		}
		if (month <= maxFebruary) {
			return true;
		}
	}

	return false;
}

bool Date::setDate(int year, int month, int day) {
	bool flag = checkDate(year, month, day);
	if (flag) {
		this->year = year;
		this->month = month;
		this->day = day;
	}
	return flag;
}

int Date::getYear() {
	return this->year;
}

int Date::getMonth() {
	return this->month;
}

int Date::getDay() {
	return this->day;
}


void Date::minusDay(int days) {
	struct tm oldTm;
	oldTm.tm_year = this->year - 1900;
	oldTm.tm_mon = this->month - 1;
	oldTm.tm_mday = this->day;
	oldTm.tm_hour = 12;
	oldTm.tm_min = 0;
	oldTm.tm_sec = 0;
	oldTm.tm_isdst = -1;
	long long oldTime = mktime(&oldTm);
	long long newTime = oldTime - days * 24L * 60L * 60L;

	struct tm newTm;
	localtime_s(&newTm, &newTime);
	this->year = newTm.tm_year + 1900;
	this->month = newTm.tm_mon + 1;
	this->day = newTm.tm_mday;
}

void Date::addDay(int days) {
	struct tm oldTm;
	oldTm.tm_year = this->year - 1900;
	oldTm.tm_mon = this->month - 1;
	oldTm.tm_mday = this->day;
	oldTm.tm_hour = 12;
	oldTm.tm_min = 0;
	oldTm.tm_sec = 0;
	oldTm.tm_isdst = -1;
	long long oldTime = mktime(&oldTm);
	long long newTime = oldTime + days * 24L * 60L * 60L;

	struct tm newTm;
	localtime_s(&newTm, &newTime);
	this->year = newTm.tm_year + 1900;
	this->month = newTm.tm_mon + 1;
	this->day = newTm.tm_mday;
}

int Date::compare(Date* d1, Date* d2) {
	if (NULL == d1 || NULL == d2) {
		return -100;
	}
	int d1_year = d1->getYear();
	int d1_month = d1->getMonth();
	int d1_day = d1->getDay();
	int d2_year = d2->getYear();
	int d2_month = d2->getMonth();
	int d2_day = d2->getDay();
	bool d1_flag = checkDate(d1_year, d1_month, d1_day);
	bool d2_flag = checkDate(d2_year, d2_month, d2_day);
	if (!d1_flag || !d2_flag) {
		return -100;
	}
	if (d1_year < d2_year) {
		return -1;
	}
	if (d1_year > d2_year) {
		return 1;
	}
	if (d1_month < d2_month) {
		return -1;
	}
	if (d1_month > d2_month) {
		return 1;
	}
	if (d1_day < d2_day) {
		return -1;
	}
	if (d1_day > d2_day) {
		return 1;
	}
	if (d1_year == d2_year && d1_month == d2_month && d1_day == d2_day) {
		return 0;
	}
	return -100;
}

