#pragma once

#include<iostream>
#include <fstream>
#include "Date.h"
#include "StringBuilder.h"

using namespace std;

class Logger
{
private:
	// 日志文件过期天数。过期的文件会被删除。
	const static int EXPIRE_DAYS = 4;
	ofstream outfile;
	// 根据文件名获取文件的生成日期。
	Date* getDate(char* fileName);

	static int getChildrenLength(char* inDirPath);
	// 读取文件夹，获取下级文件名的列表。
	// outFileList 输出参数，文件名列表。
	// outFileListLength 输出参数，文件名列表的长度。
	static void readDir(char* inDirPath, char*** outFileList, int* outFileListLength);
	

public:

	Logger();
	~Logger();


	void info(char* str);
	void info(const char* str);
	void info(StringBuilder* sb);
};

