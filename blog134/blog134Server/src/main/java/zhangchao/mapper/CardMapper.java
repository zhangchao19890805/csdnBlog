package zhangchao.mapper;

import zhangchao.domain.Card;

public interface CardMapper {
	
	
	/**
	 * 保存卡
	 * @param user
	 */
	int save(Card card);
	
	/**
	 * 根据userId 查找
	 */
	Card selectByUserId(String userId);

	void deleteByUserId(String userId);

}
