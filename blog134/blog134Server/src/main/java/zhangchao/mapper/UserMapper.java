package zhangchao.mapper;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import zhangchao.common.constant.UserCreateStatus;
import zhangchao.domain.User;

public interface UserMapper {

	int save(User user);
	
	List<User> selectList();
	
	void delete(User u);
	
	User selectById(String id);

	void update(User user);

	User selectUncheckUser(Timestamp now);
}
