package zhangchao.domain;

import java.sql.Timestamp;

/**
 * 日志的实体类
 * @author 张超
 *
 */
public class Log {
	private String id;
	
	private String content;
	
	private Timestamp datetime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getDatetime() {
		return datetime;
	}

	public void setDatetime(Timestamp datetime) {
		this.datetime = datetime;
	}
	
	
}
