package zhangchao.dao;



import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import zhangchao.domain.Log;
import zhangchao.common.db.FirstDBFactory;
import zhangchao.mapper.LogMapper;

import org.springframework.stereotype.Component;

@Component
public class LogDao {
	
	
	public void save(SqlSession sqlSession, Log log) {
			
		LogMapper mapper = sqlSession.getMapper(LogMapper.class);
		mapper.save(log);
		sqlSession.commit();
		
	}

}
