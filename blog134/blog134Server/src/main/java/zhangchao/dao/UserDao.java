package zhangchao.dao;



import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import zhangchao.common.constant.UserCreateStatus;
import zhangchao.common.db.FirstDBFactory;
import zhangchao.domain.User;
import zhangchao.mapper.UserMapper;

import org.springframework.stereotype.Component;

@Component
public class UserDao {
	
	public int save(SqlSession sqlSession, User user) {
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		int i = mapper.save(user);
		return i;
	}

	
	public List<User> selectList(SqlSession sqlSession){
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		return mapper.selectList();
	}

	public void delete(SqlSession sqlSession, String id) {
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		User u = new User();
		u.setId(id);
		mapper.delete(u);
	}
	
	public User selectById(SqlSession sqlSession, String id){
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		return mapper.selectById(id);
	}


	public void update(SqlSession sqlSession, User user) {
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		mapper.update(user);
	}


	public User selectUncheckUser(SqlSession sqlSession) {
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		Timestamp now = new Timestamp(System.currentTimeMillis() - 30L * 1000L);
		return mapper.selectUncheckUser(now);
	}
}
