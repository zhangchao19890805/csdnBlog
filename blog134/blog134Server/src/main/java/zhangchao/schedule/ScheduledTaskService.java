package zhangchao.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import zhangchao.domain.User;
import zhangchao.service.UserService;

/**
 * 单线程任务
 * @author zhangchao
 *
 */
@Service
public class ScheduledTaskService {
	@Autowired
	private UserService userService;
	
	/**
	 * fixedDelay = 1000表示当前方法执行完毕5000ms后，Spring scheduling会再次调用该方法
	 */
	@Scheduled(fixedDelay = 1000)
    public void testFixDelay() {
		User user = this.userService.selectUncheckUser();
		if (null != user) {
			this.userService.doCheckSave(user.getId());
		}
    }
	

}
