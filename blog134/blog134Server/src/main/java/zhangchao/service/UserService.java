package zhangchao.service;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zhangchao.common.db.FirstDBFactory;
import zhangchao.common.db.SecondDBFactory;
import zhangchao.common.exception.ExceptionUtils;
import zhangchao.dao.CardDao;
import zhangchao.dao.UserDao;
import zhangchao.domain.*;
import zhangchao.common.constant.UserCreateStatus;

/**
 * 用户的服务类
 * @author 张超
 *
 */
@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	@Autowired
	private CardDao cardDao;


	public List<User> selectList(){
		SqlSession sqlSession = FirstDBFactory.getInstance().openSession(true);
		List<User> r = null;
		try {
			r = this.userDao.selectList(sqlSession);
		} finally{
			sqlSession.close();
		}
		return r;
	}
	
	/**
	 * 删除用户
	 * @param id
	 */
	public void delete(String id) {
		SqlSession sqlSession = FirstDBFactory.getInstance().openSession(true);
		try {
			this.userDao.delete(sqlSession, id);
		} finally{
			sqlSession.close();
		}
	}
	
	public void save(User user, Card card) {
		// 第一个数据库，放User表
		SqlSession sqlSession_1 = FirstDBFactory.getInstance().openSession();
		// 第二个数据库，放Card表
		SqlSession sqlSession_2 = SecondDBFactory.getInstance().openSession();
		boolean firstSessionCommit = false;
		try {
			this.userDao.save(sqlSession_1, user);
			this.cardDao.save(sqlSession_2, card);
			new BigDecimal("11").divide(user.getBalance(),2);
			
			// 提交
			sqlSession_1.commit();
			firstSessionCommit = true;
			new BigDecimal("11").divide(new BigDecimal(card.getNo()), 2);
			sqlSession_2.commit();
		} catch (Exception e) {
			throw ExceptionUtils.catchException(e, 
					sqlSession_1, firstSessionCommit, ()->{
						this.delete(user.getId());
						System.out.println("调用UserService.delete方法");
					}, 
					sqlSession_2);
		} finally {
			sqlSession_1.close();
			sqlSession_2.close();
		}
	}

	/**
	 * 检查指定的UserID的用户有没有正常创建。
	 * @param userId
	 * @return true表示校验成功，数据一致性良好。false表示数据一致性有问题，校验失败。
	 *         会把 t_user 和 t_card 中的数据都删除，作为回退操作。
	 */
	public boolean doCheckSave(String userId){
		// 第一个数据库，放User表
		SqlSession sqlSession_1 = FirstDBFactory.getInstance().openSession(true);
		// 第二个数据库，放Card表
		SqlSession sqlSession_2 = SecondDBFactory.getInstance().openSession(true);
		boolean flag = false;
		try {
			User u = this.userDao.selectById(sqlSession_1, userId);
			Card card = this.cardDao.selectByUserId(sqlSession_2, userId);
			if (null != u && null != card) {
				flag = true;
			}
			// this.userDao.update(sqlSession_1, user4Update); 只有一条语句
			// 且是自动提交，没必要加回退。
			// 另一个分支的两条删除语句，因为有校验，也没必要加事务。
			if (flag) {
				User user4Update = new User();
				user4Update.setId(userId);
				user4Update.setCreateStatus(UserCreateStatus.FINISH.getValue());
				this.userDao.update(sqlSession_1, user4Update);
			} else {
				this.cardDao.deleteByUserId(sqlSession_2, userId);
				this.userDao.delete(sqlSession_1, userId);
			}
		}finally {
			sqlSession_1.close();
			sqlSession_2.close();
		}
		return flag;
	}
	
	
	public User selectUncheckUser(){
		// 第一个数据库，放User表
		SqlSession sqlSession_1 = FirstDBFactory.getInstance().openSession(true);
		try {
			User user = this.userDao.selectUncheckUser(sqlSession_1);
			return user;
		} finally {
			sqlSession_1.close();
		}
	}
}
