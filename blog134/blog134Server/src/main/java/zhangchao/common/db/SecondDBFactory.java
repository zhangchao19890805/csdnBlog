package zhangchao.common.db;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * 保存第二个数据库的SqlSessionFactory
 * @author 张超
 *
 */
public class SecondDBFactory {
	private static SqlSessionFactory sqlSessionFactory = null;
	
	static {
		String resource = "mybatis-config-2.xml";
		InputStream inputStream = null;
		try {
			inputStream = Resources.getResourceAsStream(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
	}
	
	public static SqlSessionFactory getInstance(){
		return sqlSessionFactory;
	}
}
