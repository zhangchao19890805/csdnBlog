package zhangchao.common.exception;

/**
 * 异常处理中的补偿操作接口
 * @author 张超
 *
 */
public interface Compensate {
	void compensate();
}
