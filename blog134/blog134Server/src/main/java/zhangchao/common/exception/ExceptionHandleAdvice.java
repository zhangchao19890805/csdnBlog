package zhangchao.common.exception;

import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import zhangchao.common.core.R;
import zhangchao.domain.Log;
import zhangchao.service.LogService;

/**
 * Spring Boot 1 统一封装异常
 * @author 张超
 *
 */
@RestControllerAdvice
public class ExceptionHandleAdvice {
	@Autowired
	private LogService logService;
	
	
	@ExceptionHandler(value=Throwable.class)
	public R exception(Throwable t){
		StackTraceElement[] steArr = t.getStackTrace();
		String[] details = new String[steArr.length];
		for (int i = 0; i < details.length; i++) {
			details[i] = steArr[i].toString();
		}
		String message = t.getMessage();
		StringBuilder content = new StringBuilder();
		for (String str : details) {
			content.append(str).append("\n");
		}
		content.append("\n").append(message).append("\n");
		
		Log log = new Log();
		log.setId(UUID.randomUUID().toString());
		log.setContent(content.toString());
		log.setDatetime(new Timestamp(System.currentTimeMillis()));
		// 这里加上try catch，就算数据库停止了，也能处理异常。
		try {
			logService.save(log);
		} catch (Exception e) {
			// 此处也可以改成存入日志文件中
			e.printStackTrace();
		}
		return R.error().put("msg", message);
	}
}
