package zhangchao.common.constant;

public enum UserCreateStatus {
	/**
	 * 用户创建的状态是创建中。
	 */
	CREATING(0),
	/**
	 * 用户创建的状态是创建已经完成。t_user 和 t_card表中的数据都已经插入成功。
	 */
	FINISH(1);
	private int value;
	
	private UserCreateStatus(int value){
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
}
