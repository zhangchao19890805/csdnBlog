//+------------------------------------------------------------------+
//|                                                         Zc02.mq4 |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                                 514899126@qq.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "514899126@qq.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
{
    string cookie=NULL,headers;
    char post[],result[];
    int res;
//--- to enable access to the server, you should add URL "https://www.google.com/finance"
//--- in the list of allowed URLs (Main Menu->Tools->Options, tab "Expert Advisors"):
    // string google_url="https://www.google.com/finance";
    string google_url = "http://localhost/api/operation/getNewOne";
//--- Reset the last error code
    ResetLastError();
//--- Loading a html page from Google Finance
    int timeout=5000; //--- Timeout below 1000 (1 sec.) is not enough for slow Internet connection
    res=WebRequest("GET",google_url,cookie,NULL,timeout,post,0,result,headers);
//--- Checking errors
    if(res==-1)
    {
        Print("Error in WebRequest. Error code  =",GetLastError());
        //--- Perhaps the URL is not listed, display a message about the necessity to add the address
        MessageBox("Add the address '"+google_url+"' in the list of allowed URLs on tab 'Expert Advisors'",
                "Error",MB_ICONINFORMATION);
    }
    else
    {
        string ResultStr = CharArrayToString(result, 0, -1, CP_UTF8);
        MessageBox(ResultStr,
                "info");
        
    }
   
}
//+------------------------------------------------------------------+
