//+------------------------------------------------------------------+
//|                                               ZcApiJsonUtils.mqh |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                                 514899126@qq.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "514899126@qq.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
// #include <json.mqh>

class ZcApiJsonUtils
{
    private:

    public:
        static bool isOk(string str);
        static string getActionStr(string jsonStr);
};

/*
检查API返回的字符串中是否有 "code": 200 , 如果有说明API正常返回，反之就是出错。
含有就返回true，不含就返回false。
string str : API 返回的json 字符串 
*/
bool ZcApiJsonUtils::isOk(string str){
    string codeStr = "\"code\"";
    string result[];
    int index = 0;
    // 检查是否包含 "code"
    index = StringFind(str, codeStr);
    if (index <= -1) {
        // 如果连 "code" 都没有，肯定出错了。
        return false;
    }
    // 获得 "code" 后半部分的字符串。
    string str2 = StringSubstr(str, index + StringLen(codeStr));
    // 去除掉左侧多余空格。
    string str3 = StringTrimLeft(str2);
    // 如果最左侧没有json格式必须的冒号,说明有错
    if (StringFind(str3, ":") != 0) {
        return false;
    }
    // 去掉冒号
    string str4 = StringSubstr(str3, 1);
    // 去掉左侧多余的空格
    string str5 = StringTrimLeft(str4);
    // 如果是 200，说明一切正常
    if (StringFind(str5, "200") == 0) {
        return true;
    }
    return false;
}

/*
从JSON中解析出操作代码
*/
string ZcApiJsonUtils::getActionStr(string jsonStr){
    const string A = "\"a\"";
    const string QUOTE = "\"";
    int index = StringFind(jsonStr, A);
    if (index <= -1) {
        return NULL;
    }
    // 去掉 "a"，获得后半段字符串，并去掉左侧多余空格
    string str2 = StringTrimLeft(StringSubstr(jsonStr, index + StringLen(A)));
    if (StringFind(str2, ":") != 0) {
        return NULL;
    }
    // 去掉冒号，和冒号右边的空格
    string str3 = StringTrimLeft(StringSubstr(str2, 1));
    // 去掉左侧开始的引号
    if (StringFind(str3, QUOTE) !=0 ) {
        return NULL;
    }
    string str4 = StringSubstr(str3,1);
    // 去掉结束的引号
    int endQuoteIndex = StringFind(str4, QUOTE);
    string r = StringSubstr(str4, 0, endQuoteIndex); 
    return r;
}

