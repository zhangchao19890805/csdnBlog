//+------------------------------------------------------------------+
//|                                                         Zc01.mq4 |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                                 514899126@qq.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "514899126@qq.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart() 
{
    MessageBox("High[0]="+High[0]+"  Bars="+Bars, "Hi");
    double AveragePrice = 0.0;
    double Total = 0.0;   
    for(int a = 0; a < Bars; a++)
    {     
        Total += High[a];
    }
    AveragePrice = Total / Bars;
    MessageBox("AveragePrice=" + AveragePrice, "AveragePrice");
    MessageBox(Open[0] + " " + Close[0] + " " + Open[1] + " " + Close[1], "Open Close");
    MessageBox("Ask=" + Ask+" Bid=" + Bid, "Ask Bid");
    Print("aaa", "bbb");
}
//+------------------------------------------------------------------+
