import index from './index.vue';
import redirectVue from "./redirectVue.vue";
export default [
    {
        path:'/',
        name: "index",
        component:index,
        children: [
            // redirect
            {
                path: 'r',
                name: 'redirect',
                component: redirectVue
            }
        ]
    }
]