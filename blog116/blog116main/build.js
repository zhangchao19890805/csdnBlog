var fs = require("fs");

// 更改public文件夹里面的版本号。
var str = fs.readFileSync('./public/version.json').toString();
var publicObj = JSON.parse(str);
var date = new Date();
var time = date.getTime(); // 使用时间戳作为版本号。
publicObj.data.version = time;
var outStr = JSON.stringify(publicObj);
fs.writeFileSync("./public/version.json", outStr);

// 更改客户端本地 routerVersion.js 里面的版本号。
var wstr = "const v = " + time + ";  "
wstr = wstr + "export default v;";
fs.writeFileSync("./src/routerVersion.js", wstr);
