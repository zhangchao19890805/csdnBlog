import index from './index.vue';
import routerUtils from "./routerUtils.js";

export default [
    {
        path:'/',
        name: "index",
        component:index,
        children: [
            {
                path:"home",
                name: "home",
                component: ()=>import("./home.vue").catch(routerUtils.catchImport)
            },
            {
                path: "page1",
                name: "page1",
                component: ()=>import("./page1.vue").catch(routerUtils.catchImport)
            }
        ]
    }
]