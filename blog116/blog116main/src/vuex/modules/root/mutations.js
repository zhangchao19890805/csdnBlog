export default {
    // 项目根组件 App.vue 上，窗口改变大小
    rootWindowResizeFlagMutation(state){
        state.rootWindowResizeFlag ++;
    },

    // 记录要跳转过去的路径
    routerToPathMutation(state, value){
        state.routerToPath = value;
    }

}
