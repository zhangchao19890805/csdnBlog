export default {

    // 项目根组件 App.vue 上，窗口改变大小
    rootWindowResizeFlagAction(context) {
        context.commit("rootWindowResizeFlagMutation");
    },

    // 记录要跳转过去的路径
    routerToPathAction(context, value){
        context.commit("routerToPathMutation", value);
    }
}
