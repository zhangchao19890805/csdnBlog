export default {
    
    rootWindowResizeFlag(state){
        return state.rootWindowResizeFlag;
    },

    // 得到要跳转过去的路径。
    routerToPath(state) {
        return state.routerToPath;
    }
}
