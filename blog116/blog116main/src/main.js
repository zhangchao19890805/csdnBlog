import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './router'
import 'babel-polyfill';
import store from './vuex/store';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    // 记录要跳转过去的路径，如果载入页面失败，就刷新浏览器。
    store.dispatch("routerToPathAction", to.fullPath);
    next();
});

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
