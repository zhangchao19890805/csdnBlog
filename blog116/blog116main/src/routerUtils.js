import store from "./vuex/store.js"
import axios from "axios";
import localVersion from "./routerVersion.js";

const SITE_URL = "http://localhost";
const REDIRECT_SITE = "http://localhost:8081";

export default {
    async catchImport(err){
        try {
            let res = await axios.get(SITE_URL + "/version.json", {
                params:{r: Math.random()}
            });
            if (localVersion < res.data.data.version) {
                window.location.href = REDIRECT_SITE + "/r?p=" + encodeURI(store.getters.routerToPath);
            } else {
                console.error("err", err);
            }
        } catch (err2) {
            console.error("err2", err2);
        }
    }
}
