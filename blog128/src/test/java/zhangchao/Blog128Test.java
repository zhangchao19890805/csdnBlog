package zhangchao;

import java.math.BigDecimal;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;
import java.net.URLEncoder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import zhangchao.sys.page.Pagination;
import zhangchao.sys.result.BaseResult;
import zhangchao.sys.result.FailResult;
import zhangchao.sys.result.ObjectOkResult;
import zhangchao.sys.utils.StringUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional //2
public class Blog128Test {
//	@Autowired
//	PersonRepository personRepository;
	
	MockMvc mvc;
	
	@Autowired 
	WebApplicationContext webApplicationContext;
	
	String expectedJson;
	
	@Before //3
	public void setUp() throws JsonProcessingException{ 
//		Person p1 = new Person("wyf");
//		Person p2 = new Person("wisely");
//		personRepository.save(p1);
//		personRepository.save(p2);
		
//		expectedJson =Obj2Json(personRepository.findAll()); //4
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
		
	}
	
	protected String Obj2Json(Object obj) throws JsonProcessingException{//5
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}
	
	public void testUpdate(String uri, String id) throws Exception {
		// 测试修改
		String postContent = "{\"address\":\"金陵一路\", \"balance\":7.3, \"birthday\":1481770165015, "
				+ " \"isVip\":false, \"level\":4, \"name\":\"元五\", \"id\":\""+id+"\"}";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri).content(postContent)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		int status = result.getResponse().getStatus();
		ObjectMapper mapper = new ObjectMapper();
		ObjectOkResult objectOkResult = mapper.readValue(result.getResponse().getContentAsString(), ObjectOkResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		Map<?,?> map = (Map<?,?>)objectOkResult.data;
		Assert.assertTrue(objectOkResult.code == 0);
		Assert.assertTrue(id.equals(map.get("id").toString()));
		Assert.assertTrue("金陵一路".equals(map.get("address").toString()));
		Assert.assertTrue(new BigDecimal("7.3").equals(new BigDecimal(map.get("balance").toString())));
		Assert.assertFalse(Boolean.parseBoolean(map.get("isVip").toString()));
		Assert.assertTrue(4 == Integer.parseInt(map.get("level").toString()));
		
		// 缺少ID
		postContent = "{\"address\":\"金陵一路\", \"balance\":7.3, \"birthday\":1481770165015, "
				+ " \"isVip\":false, \"level\":4, \"name\":\"元五\"}";
		result = mvc.perform(MockMvcRequestBuilders.put(uri).content(postContent)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		status = result.getResponse().getStatus();
		FailResult faiResult = mapper.readValue(result.getResponse().getContentAsString(), FailResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		Assert.assertTrue(faiResult.code == 404);
		Assert.assertEquals("错误", "缺少ID", faiResult.message);
		
		// ID不正确
		postContent = "{\"address\":\"金陵一路\", \"balance\":7.3, \"birthday\":1481770165015, "
				+ " \"isVip\":false, \"level\":4, \"name\":\"元五\", \"id\":\"aaaa\"}";
		result = mvc.perform(MockMvcRequestBuilders.put(uri).content(postContent)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		status = result.getResponse().getStatus();
		faiResult = mapper.readValue(result.getResponse().getContentAsString(), FailResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		Assert.assertTrue(faiResult.code == 404);
		Assert.assertEquals("错误", "没有这条记录", faiResult.message);
	
	}
	
	public void testDetail(String uri, String id) throws Exception {
		FailResult failResult;
		MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri+"/"+id)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		int status = result.getResponse().getStatus();
		ObjectMapper mapper = new ObjectMapper();
		ObjectOkResult objectOkResult = mapper.readValue(result.getResponse().getContentAsString(), 
				ObjectOkResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		Map<?,?> map = (Map<?,?>)objectOkResult.data;
		Assert.assertTrue(objectOkResult.code == 0);
		Assert.assertTrue(id.equals(map.get("id").toString()));
		Assert.assertTrue("金陵一路".equals(map.get("address").toString()));
		Assert.assertTrue(new BigDecimal("7.3").equals(new BigDecimal(map.get("balance").toString())));
		Assert.assertFalse(Boolean.parseBoolean(map.get("isVip").toString()));
		Assert.assertTrue(4 == Integer.parseInt(map.get("level").toString()));
		
		// 缺少ID
//		result = mvc.perform(MockMvcRequestBuilders.get(uri+"/  　  ")
String uri2 = uri + "/" + URLEncoder.encode(" 　　","UTF-8") + "啊";
uri2 = uri2.replace("+", "%20");
System.out.println(uri2);
URI u = new URI("/author/%20");
// new URI(uri+"/　啊")
MockHttpServletRequestBuilder mmrb = MockMvcRequestBuilders.get(u);
		result = mvc.perform(mmrb
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		status = result.getResponse().getStatus();
		failResult = mapper.readValue(result.getResponse().getContentAsString(), FailResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
System.out.println(uri);
System.out.println(failResult.code);
System.out.println(failResult.message);
		Assert.assertTrue(failResult.code == 404);
		Assert.assertEquals("错误", "缺少ID", failResult.message);
		
		// ID不正确
		result = mvc.perform(MockMvcRequestBuilders.get(uri+"/"+"aaa")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		status = result.getResponse().getStatus();
		failResult = mapper.readValue(result.getResponse().getContentAsString(), FailResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		Assert.assertTrue(failResult.code == 404);
		Assert.assertEquals("错误", "没有这条记录", failResult.message);
		
	}
	
	@Test
	public void testAuthor() throws Exception {
		// 测试保存
		String uri = "/author";
		String postContent = "{\"address\":\"金陵路\", \"balance\":7.2, \"birthday\":1481770165015, "
				+ " \"isVip\":true, \"level\":3, \"name\":\"元五\"}";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).content(postContent)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		int status = result.getResponse().getStatus();
		String content = result.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();  
		ObjectOkResult objectOkResult = mapper.readValue(content, ObjectOkResult.class);
		
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		Map<?,?> map = (Map<?,?>)objectOkResult.data;
		String name = String.valueOf(map.get("name"));
		String id = String.valueOf(map.get("id"));
		Assert.assertTrue(objectOkResult.code == 0);
		Assert.assertTrue(StringUtils.isNotEmpty(id));
		Assert.assertTrue("元五".equals(name));
		
		// 测试更新
		testUpdate(uri, id);
		
		// 测试详情
		testDetail(uri, id);
		
		// 测试删除
		result = mvc.perform(MockMvcRequestBuilders.delete(uri+"/"+id)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		status = result.getResponse().getStatus();
		objectOkResult = mapper.readValue(result.getResponse().getContentAsString(), 
				ObjectOkResult.class);
		Assert.assertEquals("错误，正确的返回值为200",200, status);
		map = (Map<?,?>)objectOkResult.data;
		Assert.assertTrue(objectOkResult.code == 0);
	}
	
	/*
	@SuppressWarnings("unchecked")
	@Test
	public void testPersonController() throws Exception {
		ObjectMapper mapper = new ObjectMapper();  
		String uri="/author/page";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
																.andReturn(); //6
		int status = result.getResponse().getStatus(); //7
		String content = result.getResponse().getContentAsString(); //8
		ObjectOkResult objectOkResult = mapper.readValue(content, ObjectOkResult.class);
		LinkedHashMap map = (LinkedHashMap)objectOkResult.data;
		Object obj = map.get("paging");
		System.out.println(obj);
		Assert.assertEquals("错误，正确的返回值为200",200, status); //9
//		Assert.assertTrue();
//		Assert.assertEquals("错误，返回值和预期返回值不一致", expectedJson,content); //10
	} */
}
