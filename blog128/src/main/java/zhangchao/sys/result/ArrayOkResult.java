package zhangchao.sys.result;

public class ArrayOkResult extends BaseResult {
	public String message = "success";
	public Object[] data;
	public ArrayOkResult() {
		super();
	}
	public ArrayOkResult(Object[] data) {
		super();
		this.data = data;
	}
	
	
}
