package zhangchao.sys.result;

public class ObjectOkResult extends BaseResult {
	public String message = "success";
	public Object data;
	
	public ObjectOkResult() {
		super();
	}
	
	public ObjectOkResult(Object data) {
		super();
		this.data = data;
	}
}
