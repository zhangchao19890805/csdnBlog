package zhangchao.sys.page;

public class Paging {
	public int pageNo;
	public int pageSize;
	public int pageCount;
	public int totalRecordCount;
	
	public Paging(int pageNo, int pageSize, int pageCount, int totalRecordCount) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.pageCount = pageCount;
		this.totalRecordCount = totalRecordCount;
	}

	public Paging() {
		super();
	}
	
	
}
