package zhangchao.sys.exception;

public final class NoRecordException extends RuntimeException {

	/**
	 * 序列化ID
	 */
	private static final long serialVersionUID = -2952407573113527896L;

	public NoRecordException(String message) {
		super(message);
	}

}
