package zhangchao.sys.exception;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zhangchao.sys.result.FailResult;

import springfox.documentation.annotations.ApiIgnore;

@RestController
public class MainsiteErrorController implements ErrorController {

	private static final String ERROR_PATH = "/error";  

	@RequestMapping(value=ERROR_PATH)
	public FailResult handleError(){
		String[] details = {};
		FailResult r = new FailResult(404, "无法找到该资源", details);
		return r;
	}

	@Override  
	public String getErrorPath() {
		return ERROR_PATH;  
	}

}
