package zhangchao.web;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import zhangchao.dto.ProfileUploadDTO;
import zhangchao.externalconfig.ZhangchaoSetting;
import zhangchao.sys.result.BaseResult;
import zhangchao.sys.result.ObjectOkResult;
import zhangchao.sys.utils.Base64Utils;

/**
 * 作者
 * @author jyn
 * 
 */
@RestController
@RequestMapping("/api/profile")
@Api(tags= {"profile"})
public class ProfileController {
	
	@Autowired
	private ZhangchaoSetting zhangchaoSetting;
	
	@ApiOperation(value = "上传拍照头像。", notes = "上传拍照头像。用Base64传输图片内容。", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value="/upload",method=RequestMethod.POST)
	public BaseResult upload(@RequestBody ProfileUploadDTO profileUploadDTO){
		ObjectOkResult r = new ObjectOkResult();
		Integer userId = profileUploadDTO.userId;
		String imgStr = profileUploadDTO.imgStr;
		String basePath = this.zhangchaoSetting.getUploadPath();
		String filePath = basePath + "/" + userId + ".png";
		Base64Utils.createFile(imgStr, new File(filePath));
		return r;
	}
}
