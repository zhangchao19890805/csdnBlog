#include "pch.h"
#include "JsonLexer.h"
#include <string>
#include "JsonToken.h"
#include "JsonTokenTypeConst.h"

bool JsonLexer::isWhitespace(char ch) {
	bool result = false;
	if (' ' == ch || '\r' == ch || '\n' == ch || '\t' == ch) {
		result = true;
	}
	return result;
}

int JsonLexer::endIndex_String(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 2) {
		return -1;
	}
	if ('\"' != json[startIndex]) {
		return -1;
	}
	for (int i = startIndex + 1; i < jsonSize; i++) {
		char ch = json[i];
		char next;
		if (i < jsonSize - 1) {
			next = json[i + 1];
		}
		else {
			next = ' ';
		}
		// 找到结束字符串的另一个双引号，这里考虑到了反斜杠转义的问题。
		if ('\\' == ch && '\\' == next) {
			i++;
		}
		else if ('\\' == ch && '\"' == next) {
			i++;
		} 
		else if ('\"' == ch) {
			return i;
		}
	}
	return -1;
}

int JsonLexer::endIndex_BeginArray(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 1) {
		return -1;
	}
	if ('[' == json[startIndex]) {
		return startIndex;
	}
	return -1;
}

int JsonLexer::endIndex_BeginObject(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 1) {
		return -1;
	}
	if ('{' == json[startIndex]) {
		return startIndex;
	}
	return -1;
}

int JsonLexer::endIndex_EndArray(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 1) {
		return -1;
	}
	if (']' == json[startIndex]) {
		return startIndex;
	}
	return -1;
}

int JsonLexer::endIndex_EndObject(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 1) {
		return -1;
	}
	if ('}' == json[startIndex]) {
		return startIndex;
	}
	return -1;
}

int JsonLexer::endIndex_NameSeparator(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 1) {
		return -1;
	}
	if (':' == json[startIndex]) {
		return startIndex;
	}
	return -1;
}

int JsonLexer::endIndex_ValueSeparator(char* json, int jsonSize, int startIndex) {
	int noScanSize = jsonSize - 1 - startIndex;
	if (noScanSize < 1) {
		return -1;
	}
	if (',' == json[startIndex]) {
		return startIndex;
	}
	return -1;
}

int JsonLexer::generateTokenList(char* inJson, JsonToken*** outTokenList, int& outTokenListLength) {
	int length = int(strlen(inJson));
	int max = 10;
	JsonToken** tokenList = new JsonToken * [max];
	for (int i = 0; i < max; i++) {
		tokenList[i] = NULL;
	}

	outTokenListLength = 0;
	for (int i = 0; i < length;) {
		
		int endIndex = -1;

		if ((endIndex = JsonLexer::endIndex_String(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::STRING);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else if ((endIndex = JsonLexer::endIndex_BeginArray(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::BEGIN_ARRAY);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else if ((endIndex = JsonLexer::endIndex_BeginObject(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::BEGIN_OBJECT);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else if ((endIndex = JsonLexer::endIndex_EndArray(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::END_ARRAY);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else if ((endIndex = JsonLexer::endIndex_EndObject(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::END_OBJECT);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else if ((endIndex = JsonLexer::endIndex_NameSeparator(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::NAME_SEPARATOR);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else if ((endIndex = JsonLexer::endIndex_ValueSeparator(inJson, length, i)) > -1) {
			JsonToken* tokenPtr = new JsonToken(inJson, i, endIndex, JsonTokenTypeConst::VALUE_SEPARATOR);
			tokenList[outTokenListLength] = tokenPtr;
			outTokenListLength++;
			i = 1 + endIndex;
		}
		else {
			i++;
		}

		// 如果 tokenList 空间不够，就增大。
		if (endIndex > -1 && outTokenListLength >= max) {
			int newMax = max * 2;
			JsonToken** newTokenList = new JsonToken * [newMax];

			for (int j = 0; j < max; j++) {
				newTokenList[j] = tokenList[j];
			}
			max = newMax;
			delete[] tokenList;
			tokenList = newTokenList;
			newTokenList = NULL;
		}
	}

	if (outTokenListLength > 0) {
		(*outTokenList) = tokenList;
	}
	return 1;
}


