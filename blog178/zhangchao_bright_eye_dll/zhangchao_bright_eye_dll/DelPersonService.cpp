﻿#include "pch.h"
#include "DelPersonService.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "StringBuilder.h"
#include "Logger.h"


using namespace std;

void DelPersonService::delAll(long lUserID) {
	unique_ptr<StringBuilder> sb(new StringBuilder());
	unique_ptr<Logger> logger(new Logger());

	// 处理输入结构体
	char requestUrlArr[128];
	sprintf_s(requestUrlArr, "PUT /ISAPI/AccessControl/UserInfoDetail/Delete?format=json");
	NET_DVR_XML_CONFIG_INPUT inputParam = { 0 };
	inputParam.dwSize = sizeof(inputParam);
	inputParam.lpRequestUrl = requestUrlArr;
	inputParam.dwRequestUrlLen = (unsigned long)strlen(requestUrlArr);
	char lpInBuffer[512];  // 输入的JSON
	sprintf_s(lpInBuffer, "{\"UserInfoDetail\":{\"mode\":\"all\"}}");
	inputParam.lpInBuffer = lpInBuffer;
	unsigned long dwInBufferSize = (unsigned long)strlen(lpInBuffer);
	inputParam.dwInBufferSize = dwInBufferSize;


	// 处理输出结构体。
	NET_DVR_XML_CONFIG_OUTPUT outputParam = { 0 };
	outputParam.dwSize = sizeof(outputParam);
	outputParam.dwOutBufferSize = 1024 * 1024;
	unique_ptr<char[]> lpOutBuffer{ new char[outputParam.dwOutBufferSize] };
	outputParam.lpOutBuffer = lpOutBuffer.get();
	

	logger->info("Before NET_DVR_STDXMLConfig.");
	BOOL flag = NET_DVR_STDXMLConfig(lUserID, &inputParam, &outputParam);

	if (!flag) {
		return;
	}

	// 开始建立长连接
	char startInBuf[] = "GET /ISAPI/AccessControl/UserInfoDetail/DeleteProcess?format=json";
	long startHandle = NET_DVR_StartRemoteConfig(lUserID, NET_DVR_JSON_CONFIG, startInBuf, sizeof(startInBuf), 
		NULL, NULL);

	int n = 3;
	if (-1 != startHandle ) {
		long lHandle = -1;
		do {
			const int nextOutBufMax = 100 * 1024;
			char* nextOutBuf = new char[nextOutBufMax];
			lHandle = NET_DVR_GetNextRemoteConfig(lHandle, nextOutBuf, nextOutBufMax);
			logger->info(sb->clear()->append("lHandle=")->append(lHandle));
			logger->info(sb->clear()->append("nextOutBuf=")->append(nextOutBuf));
			delete[] nextOutBuf;
			nextOutBuf = NULL;
		} while (-1 != lHandle);
	}
	NET_DVR_StopRemoteConfig(startHandle);
	

	logger->info(sb->clear()->append("outputParam.dwReturnedXMLSize=")->append(outputParam.dwReturnedXMLSize));
	logger->info(sb->clear()->append("outputParam.lpOutBuffer=")->append((char*)outputParam.lpOutBuffer));
	logger->info(sb->clear()->append("lpOutBuffer=")->append(lpOutBuffer.get()));
}