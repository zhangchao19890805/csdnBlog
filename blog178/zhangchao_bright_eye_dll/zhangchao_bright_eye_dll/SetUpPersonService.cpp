﻿#include "pch.h"
#include "SetUpPersonService.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "Logger.h"
#include "FileUtils.h"
#include "JsonUtils.h"
#include "StringBuilder.h"

using namespace std;

void SetUpPersonService::setUpOne(long lUserID, char* jsonFileName, char* picFileName) {
	unique_ptr<StringBuilder> sb(new StringBuilder());
	//日志
	unique_ptr<Logger> logger(new Logger());

	// 读取JSON
	unique_ptr<char[]> jsonByteBuffer{ FileUtils::readFile(jsonFileName) };
	unsigned long jsonSize = 1 + (unsigned long)strlen(jsonByteBuffer.get());
	unique_ptr<char[]> employeeNo{ JsonUtils::getEmployeeNo(jsonByteBuffer) };
	if (NULL == employeeNo) {
		return;
	}

	char sURL[1024] = { "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json" };
	long m_lSetCardCfgHandle = NET_DVR_StartRemoteConfig(lUserID, NET_DVR_JSON_CONFIG, &sURL, sizeof(sURL), 
		NULL, NULL);
	if (-1 == m_lSetCardCfgHandle) {
		logger->info("SetUpPersonService::setUpOne  -1 == m_lSetCardCfgHandle ");
		return;
	} 
	else
	{
		logger->info(sb->clear()->append("SetUpPersonService::setUpOne  m_lSetCardCfgHandle=")->append(m_lSetCardCfgHandle));
	}

//	char* strUserJson = new char[1024];
//	sprintf_s(strUserJson, 1024, "{\"UserInfo\":{\"employeeNo\":\"abc01234567890123456789\",\"name\":\"aaaa\",\"userType\":\"normal\",\"Valid\":{\"enable\":true,\"beginTime\":\"2022-09-06T00:00:00\",\"endTime\":\"2037-09-06T23:59:59\",\"timeType\":\"local\"},\"belongGroup\":\"\"}}");
//	char sUserRecord[1024];
//	strcpy_s(sUserRecord, strUserJson);
//	delete[] strUserJson;

	

	char lpOutBuff[1024 * 4] = { 0 };
	DWORD dwOutDataLen = 0;
	long dwState = -1;
	while (true)
	{
		// dwState = NET_DVR_SendWithRecvRemoteConfig(m_lSetCardCfgHandle, &sUserRecord, sizeof(sUserRecord), &lpOutBuff, sizeof(lpOutBuff), &dwOutDataLen);
		dwState = NET_DVR_SendWithRecvRemoteConfig(m_lSetCardCfgHandle, jsonByteBuffer.get(), jsonSize, &lpOutBuff, sizeof(lpOutBuff), &dwOutDataLen);
		if (dwState == -1) {
			logger->info(sb->clear()->append("NET_DVR_SendWithRecvRemoteConfig")->appendGbkToUtf8("接口调用失败，错误码：")->append(NET_DVR_GetLastError()));
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_NEEDWAIT)
		{
			logger->info(sb->clear()->appendGbkToUtf8("配置等待"));
			Sleep(10);
			continue;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_FAILED)
		{
			sb->clear()->appendGbkToUtf8("下发人员失败: ")->append(lpOutBuff);
			logger->info(sb.get());
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_EXCEPTION)
		{
			logger->info(sb->clear()->appendGbkToUtf8("下发人员异常: ")->append(lpOutBuff));
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_SUCCESS)
		{
			logger->info(sb->clear()->appendGbkToUtf8("下发人员成功: ")->append(lpOutBuff));
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_FINISH) {
			logger->info(sb->clear()->appendGbkToUtf8("下发人员完成"));
			break;
		}

	}
	if (!NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle)) {
		logger->info(sb->clear()->append("NET_DVR_StopRemoteConfig")->appendGbkToUtf8("接口调用失败，错误码：")->append(NET_DVR_GetLastError()));
	}
	else {
		logger->info(sb->clear()->append("NET_DVR_StopRemoteConfig")->appendGbkToUtf8("接口成功"));
	}

	Sleep(500);

	//下发人脸图片
	char sFaceURL[1024] = { "PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json" };

	m_lSetCardCfgHandle = NET_DVR_StartRemoteConfig(lUserID, NET_DVR_FACE_DATA_RECORD, &sFaceURL, sizeof(sFaceURL), NULL, NULL);
	if (m_lSetCardCfgHandle == -1)
	{
		logger->info(sb->clear()->appendGbkToUtf8("建立下发人脸参数长连接失败，错误码为：")->append(NET_DVR_GetLastError()));
		return;
	}
	else
	{
		logger->info(sb->clear()->appendGbkToUtf8("建立下发人脸参数长连接成功！"));
	}


	NET_DVR_JSON_DATA_CFG struUserRecord = { 0 };
	struUserRecord.dwSize = sizeof(struUserRecord);



	// char sFaceInfoRecord[256] = { "{\"faceLibType\":\"blackFD\",\"FDID\":\"1\",\"FPID\":\"abc01234567890123456789\"}" };	//人脸信息JSON数据
	char sFaceInfoRecord[256];	//人脸信息JSON数据
	sprintf_s(sFaceInfoRecord, 256, "{\"faceLibType\":\"blackFD\",\"FDID\":\"1\",\"FPID\":\"%s\"}", employeeNo.get());
	struUserRecord.lpJsonData = (char*)sFaceInfoRecord;
	struUserRecord.dwJsonDataSize = (unsigned long)strlen(sFaceInfoRecord);

	FILE* fp = NULL;
	char* byteBuffer = NULL;
	int jpgSize = 0;
	errno_t err = fopen_s(&fp, picFileName, "rb");

	if (0 != err)
	{
		sb->clear()->appendGbkToUtf8("读取图片")->append(picFileName)->appendGbkToUtf8("失败");
		logger->info(sb.get());
		return;
	}
	if (0 == err)
	{
		//求得文件的大小  
		fseek(fp, 0, SEEK_END);
		jpgSize = ftell(fp);
		rewind(fp);
		byteBuffer = new char[jpgSize + 1];
		fread(byteBuffer, jpgSize, 1, fp);
		struUserRecord.lpPicData = byteBuffer;
		struUserRecord.dwPicDataSize = jpgSize;
		sb->clear()->appendGbkToUtf8("读取图片")->append(picFileName)
			->appendGbkToUtf8("成功，图片大小：")->append(jpgSize);
		logger->info(sb.get());
		fclose(fp);
		fp = NULL;

	}
	char lpFaceOutBuff[1024 * 8] = { 0 };
	DWORD dwFaceOutDataLen = 0;
	while (true)
	{
		dwState = NET_DVR_SendWithRecvRemoteConfig(m_lSetCardCfgHandle, &struUserRecord, sizeof(struUserRecord), &lpFaceOutBuff, sizeof(lpFaceOutBuff), &dwFaceOutDataLen);
		if (dwState == -1) {
			sb->clear()->append("NET_DVR_SendWithRecvRemoteConfig")->appendGbkToUtf8("接口调用失败，错误码：")->append(NET_DVR_GetLastError());
			logger->info(sb.get());
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_NEEDWAIT)
		{
			logger->info(sb->clear()->appendGbkToUtf8("配置等待"));
			Sleep(10);
			continue;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_FAILED)
		{
			logger->info(sb->clear()->appendGbkToUtf8("下发人脸失败：")->append(lpFaceOutBuff));
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_EXCEPTION)
		{
			logger->info(sb->clear()->appendGbkToUtf8("下发人脸异常：")->append(lpFaceOutBuff));
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_SUCCESS)
		{
			logger->info(sb->clear()->appendGbkToUtf8("下发人脸成功：")->append(lpFaceOutBuff));
			break;
		}
		else if (dwState == NET_SDK_CONFIG_STATUS_FINISH) {
			logger->info(sb->clear()->appendGbkToUtf8("下发人脸完成"));
			break;
		}

	}
	if (!NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle)) {
		logger->info(sb->clear()->append("NET_DVR_StopRemoteConfig")->appendGbkToUtf8("接口调用失败，错误码：")->append(NET_DVR_GetLastError()));
	}
	else {
		logger->info(sb->clear()->append("NET_DVR_StopRemoteConfig")->appendGbkToUtf8("接口成功"));
	}

	//释放指针资源
	if (NULL != byteBuffer) {
		delete[] byteBuffer;
		byteBuffer = NULL;
	}
}

void SetUpPersonService::setUpAll(long lUserID) {
	unique_ptr<Logger> logger(new Logger);
	// 读取文件夹中的头像文件和JSON文件路径
	char*** faceList = NULL;
	int faceListLength = 0;
	int readFaceFlag = FileUtils::readJsonPic(&faceList, &faceListLength);

	if (1 == readFaceFlag) {
		for (int i = 0; i < faceListLength; i++) {
			setUpOne(lUserID, faceList[i][0], faceList[i][1]);
			//char* jsonStr = FileUtils::readFile(faceList[i][0]);
			//logger->outfile << "jsonStr=" << jsonStr << endl;
			//char* employeeNo = JsonUtils::getEmployeeNo(jsonStr);

			//if (NULL == employeeNo) {
			//	logger->outfile << "employeeNo is NULL" << endl;
			//}
			//else {
			//	logger->outfile << "employeeNo=" << employeeNo << endl;
			//}
			//
			//if (NULL != employeeNo) {
			//	delete[] employeeNo;
			//}
			//if (NULL != jsonStr) {
			//	delete[] jsonStr;
			//}
		}
	}
	else if (0 == readFaceFlag) {
		logger->info("NULL == xmlList || NULL == picList");
	}
	else if (-1 == readFaceFlag) {
		logger->info("When outFaceList is written, buffer overflow.");
	}
	
	// 释放头像文件和JSON文件路径
	if (NULL != faceList) {
		for (int indexLv1 = 0; indexLv1 < faceListLength; indexLv1++) {
			if (NULL != faceList[indexLv1]) {
				for (int indexLv2 = 0; indexLv2 < 2; indexLv2++) {
					if (NULL != faceList[indexLv1][indexLv2]) {
						delete[] faceList[indexLv1][indexLv2];
						faceList[indexLv1][indexLv2] = NULL;
					}
				}
				delete[] faceList[indexLv1];
				faceList[indexLv1] = NULL;
			}
		}
		delete[] faceList;
	}
}