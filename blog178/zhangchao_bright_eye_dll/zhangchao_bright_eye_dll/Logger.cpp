#include "pch.h"
#include "Logger.h"

#include <stdio.h>
#include "Date.h"
#include "FileUtils.h"
#include "Datetime.h"


Date* Logger::getDate(char* fileName) {
	char* dateStr = new char[9];
	int beginIndex = -1;
	int endIndex = -1;
	int length = int(strlen(fileName));
	for (int i = 0; i < length; i++) {
		char c = fileName[i];
		if (i < length - 1) {
			char next = fileName[i + 1];
			if ('l' == c && 'l' == next) {
				beginIndex = i + 2;
			}
		}
		if ('.' == c) {
			endIndex = i;
		}
	}

	int dateStrIndex = 0;
	for (int i = beginIndex; i <= endIndex; i++) {
		dateStr[dateStrIndex] = fileName[i];
		dateStrIndex++;
	}
	dateStr[8] = '\0';

	char* yearStr = new char[5];
	for (int i = 0; i < 4; i++) {
		yearStr[i] = dateStr[i];
	}
	yearStr[4] = '\0';
	char* monthStr = new char[3];
	for (int i = 0; i < 2; i++) {
		monthStr[i] = dateStr[4 + i];
	}
	monthStr[2] = '\0';
	char* dayStr = new char[3];
	for (int i = 0; i < 2; i++) {
		dayStr[i] = dateStr[6 + i];
	}
	dayStr[2] = '\0';

	int year = atoi(yearStr);
	int month = atoi(monthStr);
	int day = atoi(dayStr);

	// 释放内存。
	if (NULL != dayStr) {
		delete[] dayStr;
	}
	if (NULL != monthStr) {
		delete[] monthStr;
	}
	if (NULL != yearStr) {
		delete[] yearStr;
	}
	if (NULL != dateStr) {
		delete[] dateStr;
	}
	Date* d = new Date(false);
	d->setDate(year, month, day);
	return d;
}

Logger::Logger() {
	Date today(true);
	char outfileName[256];
	char folderPath[] = "D:\\your_brighteye_log_folder\\";
	sprintf_s(outfileName, "%sbright_eye_dll%4.4d%2.2d%2.2d.txt", folderPath,
		today.getYear(), today.getMonth(), today.getDay());
	this->outfile;
	this->outfile.open(outfileName, ios::binary | ios::app | ios::in | ios::out);
	Date expire(true);
	expire.minusDay(Logger::EXPIRE_DAYS);

	unique_ptr<StringBuilder> sb(new StringBuilder);
	sb->clear()->append(folderPath)->append("bright_eye_dll*.txt");
	char* txtList = sb->toString();
	char** textFileList = NULL;
	int txtFileListLength = 0;
	FileUtils::readDir(txtList, &textFileList, &txtFileListLength);

	if (NULL != textFileList) {
		for (int i = 0; i < txtFileListLength; i++) {
			char* textFileName = textFileList[i];
			unique_ptr<Date> fileDate(getDate(textFileName));
			if (-1 == Date::compare(fileDate.get(), &expire)) {
				int absoluteFileNameMax = int(strlen(outfileName)) + 1;
				char* absoluteFileName = new char[1 + absoluteFileNameMax];
				sprintf_s(absoluteFileName, absoluteFileNameMax, "%s%s", folderPath, textFileName);
				remove(absoluteFileName);
				delete[] absoluteFileName;
			}
		}
	}

	// 释放资源。
	if (NULL != textFileList) {
		for (int i = 0; i < txtFileListLength; i++) {
			if (NULL != textFileList[i]) {
				delete[] textFileList[i];
			}
		}
		delete[] textFileList;
	}
	if (NULL != txtList) {
		delete[] txtList;
	}
}

Logger::~Logger() {
	this->outfile.close();
}




void Logger::info(char* str) {
	unique_ptr<Datetime> now(new Datetime());
	size_t len = strlen(str);
	char* logStr = new char[len + 64];
	sprintf_s(logStr, len + 64, "%d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d  %s\n", now->getYear(), now->getMonth(),
		now->getDay(), now->getHour(), now->getMinute(), now->getSecond(), str);
	outfile << logStr << endl;
	delete[] logStr;
}

void Logger::info(const char* str) {
	unique_ptr<Datetime> now(new Datetime());
	size_t len = strlen(str);
	char* logStr = new char[len + 64];
	sprintf_s(logStr, len + 64, "%d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d  %s\n", now->getYear(), now->getMonth(),
		now->getDay(), now->getHour(), now->getMinute(), now->getSecond(), str);
	outfile << logStr;
	delete[] logStr;
}

void Logger::info(StringBuilder* sb) {
	char* str = (*sb).toString();
	info(str);
	delete[] str;
}
