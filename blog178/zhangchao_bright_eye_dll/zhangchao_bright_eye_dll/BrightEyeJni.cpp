﻿#include "pch.h"
#include "jni.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include "com_rzfk_jni_BrightEyeJni.h"
#include "Logger.h"
#include "DelPersonService.h"
#include "SetUpPersonService.h"
#include "FileUtils.h"
#include "StringBuilder.h"

using namespace std;


// 设备登录
int DoLogin(const char* ip)
{
	unique_ptr<StringBuilder> sb(new StringBuilder());
	// 日志
	unique_ptr<Logger> logger(new Logger());

	NET_DVR_USER_LOGIN_INFO struLoginInfo = { 0 };
	struLoginInfo.bUseAsynLogin = 0; //同步登录方式
	strcpy_s(struLoginInfo.sDeviceAddress, ip); //设备IP地址
	struLoginInfo.wPort = 8000; //设备服务端口
	strcpy_s(struLoginInfo.sUserName, "yourusername"); //设备登录用户名
	strcpy_s(struLoginInfo.sPassword, "yourpassword"); //设备登录密码
	//设备参数结构体
	NET_DVR_DEVICEINFO_V40 DeviceInfoTmp;
	memset(&DeviceInfoTmp, 0, sizeof(NET_DVR_DEVICEINFO_V40));
	// 向设备注册
	int lLoginID = NET_DVR_Login_V40(&struLoginInfo, &DeviceInfoTmp);
	if (lLoginID < 0)
	{
		sb->clear()->appendGbkToUtf8("注册失败, 错误码: ")->append(NET_DVR_GetLastError());
		logger->info(sb.get());
		NET_DVR_Cleanup();
	}
	if (lLoginID >= 0)
	{
		logger->info(sb->clear()->appendGbkToUtf8("注册成功"));
	}
	return lLoginID;
}



JNIEXPORT jint 
JNICALL Java_com_rzfk_jni_BrightEyeJni_updateAllPerson(JNIEnv* env, jobject obj, jstring ip) {
	const char* ipChPtr = env->GetStringUTFChars(ip, NULL);
	unique_ptr<StringBuilder> sb(new StringBuilder());
	// 日志  bright_eye_dll
	unique_ptr<Logger> logger(new Logger());
	NET_DVR_Init();
	// 开启海康日志
	char arr[] = "D:\\your_hk_brighteye_log_folder\\";
	char* logFileDir = arr;
	NET_DVR_SetLogToFile(3, logFileDir, FALSE);
	//设置连接时间与重连时间
	NET_DVR_SetConnectTime(2000, 1);
	NET_DVR_SetReconnect(10000, true);

	int lUserID = DoLogin(ipChPtr);
	if (lUserID < 0)
	{
		// 清理IP占用的资源。
		env->ReleaseStringUTFChars(ip, ipChPtr);
		return -1;
	}
	sb->clear()->append("lUserID=")->append(lUserID);
	logger->info(sb.get());

	// 删除以前的人员数据
	DelPersonService::delAll(lUserID);

	// 上传人员
	SetUpPersonService::setUpAll(lUserID);


//	********* BEGIN 清理资源 *********** 
	//注销用户
	NET_DVR_Logout(lUserID);
	//释放SDK资源
	NET_DVR_Cleanup();
	// 清理IP占用的资源。
	env->ReleaseStringUTFChars(ip, ipChPtr);
//	********* END 清理资源 ***********


	return 1;
}

