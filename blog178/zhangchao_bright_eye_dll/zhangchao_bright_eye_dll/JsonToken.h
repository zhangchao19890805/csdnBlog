#pragma once
class JsonToken
{
private:
	int beginIndex;
	int endIndex;

public:
	int tokenType;
	char* text;

	JsonToken(char* json, int beginIndex, int endIndex, int tokenType);
	~JsonToken();
};

