#pragma once
#include "JsonToken.h"

class JsonLexer
{
private:
	static int endIndex_String(char* json, int jsonSize, int startIndex);

	static int endIndex_BeginArray(char* json, int jsonSize, int startIndex);
	static int endIndex_BeginObject(char* json, int jsonSize, int startIndex);
	static int endIndex_EndArray(char* json, int jsonSize, int startIndex);
	static int endIndex_EndObject(char* json, int jsonSize, int startIndex);
	static int endIndex_NameSeparator(char* json, int jsonSize, int startIndex);
	static int endIndex_ValueSeparator(char* json, int jsonSize, int startIndex);

public:
	// 判断是不是空白字符串
	static bool isWhitespace(char ch);

	// 生成Token数组
	static int generateTokenList(char* inJson, JsonToken*** outTokenList, int& outTokenListLength);
};

