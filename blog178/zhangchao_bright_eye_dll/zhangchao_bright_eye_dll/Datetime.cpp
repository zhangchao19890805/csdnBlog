#include "pch.h"
#include "Datetime.h"

#include <time.h>
#include <stdio.h>

using namespace std;

Datetime::Datetime() {
	struct tm t;   //tm结构指针
	time_t now;  //声明time_t类型变量
	time(&now);      //获取系统日期和时间
	localtime_s(&t, &now);   //获取当地日期和时间

	this->year = t.tm_year + 1900;
	this->month = t.tm_mon + 1;
	this->day = t.tm_mday;
	this->hour = t.tm_hour;
	this->minute = t.tm_min;
	this->second = t.tm_sec;
	this->m_time = now;
}

Datetime::Datetime(long long utcTime) {
	struct tm t;
	localtime_s(&t, &utcTime);
	this->year = t.tm_year + 1900;
	this->month = t.tm_mon + 1;
	this->day = t.tm_mday;
	this->hour = t.tm_hour;
	this->minute = t.tm_min;
	this->second = t.tm_sec;
	this->m_time = utcTime;
}

Datetime::Datetime(int year, int month, int day, int hour, int minute, int second) {
	this->year = year;
	this->month = month;
	this->day = day;
	this->hour = hour;
	this->minute = minute;
	this->second = second;

	struct tm struTm;
	struTm.tm_year = this->year - 1900;
	struTm.tm_mon = this->month - 1;
	struTm.tm_mday = day;
	struTm.tm_hour = hour;
	struTm.tm_min = minute;
	struTm.tm_sec = second;
	time_t ti = mktime(&struTm);
	this->m_time = ti;
}

Datetime::~Datetime() {

}


int Datetime::getYear() {
	return this->year;
}

int Datetime::getMonth() {
	return this->month;
}

int Datetime::getDay() {
	return this->day;
}

int Datetime::getHour() {
	return this->hour;
}

int Datetime::getMinute() {
	return this->minute;
}

int Datetime::getSecond() {
	return this->second;
}

long long Datetime::getTime() {
	return this->m_time;
}

