#include "pch.h"
#include "JsonToken.h"


JsonToken::JsonToken(char* json, int beginIndex, int endIndex, int tokenType) {
	this->beginIndex = beginIndex;
	this->endIndex = endIndex;
	this->tokenType = tokenType;

	int max = endIndex - beginIndex + 2;
	this->text = new char[max];
	for (int i = beginIndex; i <= endIndex; i++) {
		char ch = json[i];
		int textIndex = i - beginIndex;
		this->text[textIndex] = ch;
	}
	this->text[max - 1] = '\0';

}


JsonToken::~JsonToken() {
	if (NULL != text) {
		delete[] text;
	}
}