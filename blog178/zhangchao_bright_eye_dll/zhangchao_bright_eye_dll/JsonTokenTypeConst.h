#pragma once
class JsonTokenTypeConst
{
public:
	static const int BEGIN_ARRAY = 1;
	static const int BEGIN_OBJECT = 2;
	static const int END_ARRAY = 3;
	static const int END_OBJECT = 4;
	static const int NAME_SEPARATOR = 5;
	static const int VALUE_SEPARATOR = 6;
	static const int STRING = 7;
	static const int NUMBER = 8;

	// ������literal
	static const int LITERAL_TRUE = 9;
	static const int LITERAL_FALSE = 10;
	static const int LITERAL_NULL = 10;
};

