#include "pch.h"
#include "JsonUtils.h"
#include <string>
#include "JsonToken.h"
#include "JsonTokenTypeConst.h"
#include "JsonLexer.h"
#include "Logger.h"
#include "StringBuilder.h"

using namespace std;

char* JsonUtils::getEmployeeNo(unique_ptr<char[]> &json) {
	unique_ptr<StringBuilder> sb{ new StringBuilder() };
	unique_ptr<Logger> logger{new Logger()};
	if (NULL == json) {
		return NULL;
	}
	size_t jsonLength = strlen(json.get());
	if (jsonLength <= 0) {
		return NULL;
	}

	char* result = NULL;

	JsonToken** tokenList = NULL;
	int tokenListLength = 0;

	JsonLexer::generateTokenList(json.get(), &tokenList, tokenListLength);
	if (NULL != tokenList) {
		logger->info("TokenList:");
		char employeeNoStr[128] = { "\"employeeNo\"" }; // json中employeeNo键
		char emptyStr[3] = {"\"\""};  // json中空字符串
		sb->clear();
		for (int i = 0; i < tokenListLength; i++) {
			JsonToken* tokenPtr = tokenList[i];
			JsonToken* next1 = NULL;
			if (i + 1 < tokenListLength) {
				next1 = tokenList[i + 1];
			}
			JsonToken* next2 = NULL;
			if (i + 2 < tokenListLength) {
				next2 = tokenList[i + 2];
			}

			if (JsonTokenTypeConst::STRING == tokenPtr->tokenType && 
				NULL != next1 && JsonTokenTypeConst::NAME_SEPARATOR == next1->tokenType &&
				NULL != next2 && JsonTokenTypeConst::STRING == next2->tokenType &&
				0 == strcmp(employeeNoStr, tokenPtr->text) &&
				0 != strcmp(emptyStr, tokenPtr->text)
			)
			{
				size_t length = strlen(next2->text);
				result = new char[length - 1];
				for (int next2Idx = 1; next2Idx <= length - 2; next2Idx++) {
					result[next2Idx - 1] = next2->text[next2Idx];
				}
				result[length - 2] = '\0';
				
			}
			sb->append("(")->append(tokenList[i]->text)->append(")");
		}
		logger->info(sb.get());
		logger->info("\nEnd TokenList.");
	}


	// 释放资源
	if (NULL != tokenList) {
		for (int i = 0; i < tokenListLength; i++) {
			if (NULL != tokenList[i]) {
				delete tokenList[i];
			}
		}
		delete[] tokenList;
	}
	logger->info(sb->clear()->append("getEmployeeNo return ")->append(result));
	return result;
}