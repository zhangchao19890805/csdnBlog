#pragma once

#include<iostream>
#include <fstream>
#include "Date.h"
#include "StringBuilder.h"

using namespace std;


class Logger
{
private:
	// 日志文件过期天数。过期的文件会被删除。
	const static int EXPIRE_DAYS = 4;
	ofstream outfile;
	// 根据文件名获取文件的生成日期。
	Date* getDate(char* fileName);

public:

	Logger();
	~Logger();


	void info(char* str);
	void info(const char* str);
	void info(StringBuilder* sb);
};

