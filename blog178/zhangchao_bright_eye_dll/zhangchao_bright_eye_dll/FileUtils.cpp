#include "pch.h"
#include "FileUtils.h"


#include <stdlib.h>
#include <string>
#include <stdio.h>
#include<io.h>
#include "StringBuilder.h"
#include "Logger.h"
#include <functional>

int FileUtils::getChildrenLength(char* inDirPath) {
    int result = 0;
    //文件存储信息结构体 
    struct _finddata_t fileinfo;
    //保存文件句柄 
    intptr_t fHandle;

    if ((fHandle = _findfirst(inDirPath, &fileinfo)) == -1L)
    {
        result = 0;
    }
    else {
        do {
            result++;
        } while (_findnext(fHandle, &fileinfo) == 0);
    }
    //关闭文件 
    _findclose(fHandle);
    printf("文件数量：%d\n", result);
    return result;
}

void FileUtils::readDir(char* inDirPath, char*** outFileList, int* outFileListLength) {
    int fileListLength = FileUtils::getChildrenLength(inDirPath);
    (*outFileListLength) = fileListLength;
    if (fileListLength > 0) {
        (*outFileList) = new char* [fileListLength];
        //文件存储信息结构体 
        struct _finddata_t fileinfo;
        //保存文件句柄 
        intptr_t fHandle;
        //文件数记录器
        int i = 0;

        if ((fHandle = _findfirst(inDirPath, &fileinfo)) == -1L)
        {
            printf("当前目录下没有文件\n");
        }
        else {
            do {

                int fileNameSize = int(strlen(fileinfo.name));
                char* fileName = new char[fileNameSize + 1];
                for (int fileNameIndex = 0; fileNameIndex < fileNameSize; fileNameIndex++) {
                    char c = fileinfo.name[fileNameIndex];
                    fileName[fileNameIndex] = c;
                }
                fileName[fileNameSize] = '\0';
                (*outFileList)[i] = fileName;
                i++;
                //printf("找到文件:%s,文件大小：%d\n", fileinfo.name, fileinfo.size);
            } while (_findnext(fHandle, &fileinfo) == 0);
        }
        //关闭文件 
        _findclose(fHandle);
    }
} // end void FileUtils::readDir


char* FileUtils::getFileMainName(char* fileName) {
    int len = int(strlen(fileName));
    int max = 0;
    for (int i = 0; i < len; i++) {
        char c = fileName[i];
        if ('.' == c) {
            break;
        }
        else {
            max++;
        }
    }
    if (max <= 0) {
        return NULL;
    }
    max++;

    char* result = new char[max];
    for (int i = 0; i < len; i++) {
        char c = fileName[i];
        if ('.' == c) {
            break;
        }
        else {
            result[i] = c;
        }
    }
    result[max - 1] = '\0';
    return result;
}

int FileUtils::findIndex(char** picList, const int picListLength, const char* jsonFileMainName) {
    int result = -1;
    for (int i = 0; i < picListLength; i++) {
        char* picFileMainName = getFileMainName(picList[i]);
        if (NULL != picFileMainName && 0 == strcmp(jsonFileMainName, picFileMainName)) {
            result = i;
        }
        delete[] picFileMainName;
    }
    return result;
}

int FileUtils::getJsonPicLength(char** jsonList, int jsonListLength, char** picList, int picListLength) {
    int result = 0;
    for (int jsonIndex = 0; jsonIndex < jsonListLength; jsonIndex++) {
        char* jsonFileName = jsonList[jsonIndex];
        char* jsonFileMainName = getFileMainName(jsonFileName);

        if (findIndex(picList, picListLength, jsonFileMainName) > -1) {
            result++;
        }

        delete[] jsonFileMainName;
    }
    return result;
}

int FileUtils::readJsonPic(char**** outFaceList, int* outFaceListLength) {
    // json文件名列表
    char jsonDirExp[] = "D:\\your_json\\*.json";
    char** jsonList = NULL;
    int jsonListLength = 0;
    readDir(jsonDirExp, &jsonList, &jsonListLength);
    if (NULL == jsonList) {
        return 0;
    }
    // 用智能指针封装二级指针 jsonList ，确保任何时候都能回收二级指针 jsonList。
    unique_ptr<char* [], function<void(char**)>> upJsonList{ jsonList, 
        [&jsonListLength](char** list) {
            if (NULL != list) {
                for (int i = 0; i < jsonListLength; i++) {
                    char* p = list[i];
                    if (NULL != p) {
                        delete[] p;
                        list[i] = NULL;
                    }
                }
                delete[] list;
            }
        } 
    };

    // 图片文件名列表
    char picDirExp[] = "D:\\your_img\\*.*";
    char** picList = NULL;
    int picListLength = 0;
    readDir(picDirExp, &picList, &picListLength);
    if (NULL == picList) {
        return 0;
    }
    // 用智能指针封装二级指针 picList ，确保任何时候都能回收二级指针 picList。
    unique_ptr<char* [], function<void(char**)>> upPicList{picList, 
        [&picListLength](char** list) {
            if (NULL != list) {
                for (int i = 0; i < picListLength; i++) {
                    if (NULL != list[i]) {
                        delete[] list[i];
                        list[i] = NULL;
                    }
                }
                delete[] list;
            }
        }
    };


    int jsonPicListLength = getJsonPicLength(upJsonList.get(), jsonListLength, upPicList.get(), picListLength);
    (*outFaceListLength) = jsonPicListLength;
    if (jsonPicListLength > 0) {
        (*outFaceList) = new char** [jsonPicListLength];
        for (int indexLv1 = 0; indexLv1 < jsonPicListLength; indexLv1++) {
            (*outFaceList)[indexLv1] = new char* [2];
        }

        // 用于拼接json和图片文件的全名
        char jsonDirPath[] = "D:\\your_json\\";
        char picDirPath[] = "D:\\your_img\\img\\";

        int faceIndex = 0;
        for (int jsonIndex = 0; jsonIndex < jsonListLength; jsonIndex++) {
            char* jsonFileName = upJsonList[jsonIndex];
            char* jsonFileMainName = getFileMainName(jsonFileName);

            // 找到同名的图片
            int index = findIndex(upPicList.get(), picListLength, jsonFileMainName);
            if (index > -1) {
                char* picFileName = upPicList[index];
                if (faceIndex >= jsonPicListLength) {
                    return -1;
                }

                size_t jsonFullMax = strlen(jsonDirPath) + strlen(jsonFileName) + 1;
                (*outFaceList)[faceIndex][0] = new char[jsonFullMax];
                size_t picFullMax = strlen(picDirPath) + strlen(picFileName) + 1;
                (*outFaceList)[faceIndex][1] = new char[picFullMax];

                sprintf_s((*outFaceList)[faceIndex][0], jsonFullMax, "%s%s", jsonDirPath, jsonFileName);
                sprintf_s((*outFaceList)[faceIndex][1], picFullMax, "%s%s", picDirPath, picFileName);

                faceIndex++;
            }

            delete[] jsonFileMainName;
        }
    }

    return 1;
}


char* FileUtils::readFile(char* filePath) {
    unique_ptr<StringBuilder> sb(new StringBuilder());
    unique_ptr<Logger> logger(new Logger());

    FILE* jsonFilePtr = NULL;
    char* jsonByteBuffer = NULL;
    int jsonSize = 0;
    errno_t errJson = fopen_s(&jsonFilePtr, filePath, "rb");
    if (0 != errJson)
    {
        logger->info(sb->clear()->append("Fail to read json file. Json file name=")->append(filePath));
        return NULL;
    }
    if (0 == errJson)
    {
        //求得文件的大小  
        fseek(jsonFilePtr, 0, SEEK_END);
        jsonSize = ftell(jsonFilePtr);
        rewind(jsonFilePtr);
        jsonByteBuffer = new char[jsonSize + 1];
        fread(jsonByteBuffer, jsonSize, 1, jsonFilePtr);
        fclose(jsonFilePtr);
        jsonFilePtr = NULL;
    }

    return jsonByteBuffer;
}

