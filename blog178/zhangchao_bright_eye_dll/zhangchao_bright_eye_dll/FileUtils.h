#pragma once
class FileUtils
{
private:
	static int getChildrenLength(char* inDirPath);

	static int getJsonPicLength(char** jsonList, int jsonListLength, char** picList, int picListLength);

	static int findIndex(char** picList, const int picListLength, const char* str);

public:
	// 读取文件夹，获取下级文件名的列表。
	// outFileList 输出参数，文件名列表。
	// outFileListLength 输出参数，文件名列表的长度。
	static void readDir(char* inDirPath, char*** outFileList, int* outFileListLength);

	// 读取用于上传人员信息和人脸图片的列表。
	// outFaceList: 输出参数。
	//              一级指针是文件名
	//              二级指针是 new char[2] ,第0个元素是json文件名，第1个元素是图片文件名。
	//              三级指针是列表，包含若干文件对。
	//              四级指针是输出参数。
	// outFaceListLength：outFaceList的长度。
	static int readJsonPic(char**** outFaceList, int* outFaceListLength);

	// 取得文件主名。比如输入 aa.txt，返回aa。
	static char* getFileMainName(char* fileName);

	// 读取文件
	static char* readFile(char* filePath);
};

