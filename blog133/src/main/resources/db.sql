use ledongxinxi;

CREATE TABLE `t_user` (
  `c_userid` int(11) NOT NULL,
  `c_username` varchar(45) DEFAULT NULL,
  `c_usernick` varchar(45) DEFAULT NULL,
  `c_sex` varchar(4) DEFAULT NULL,
  `c_money` int(11) DEFAULT NULL,
  `c_exp` int(11) DEFAULT NULL,
  `c_userhead` varchar(300) DEFAULT NULL,
  `c_schoolid` int(11) DEFAULT NULL,
  `c_schoolname` varchar(45) DEFAULT NULL,
  `c_collegename` varchar(45) DEFAULT NULL,
  `c_classname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`c_userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `t_step` (
  `c_id` varchar(80) NOT NULL,
  `c_sport_steps` int(11) NOT NULL,
  `c_date_time` date NOT NULL,
  `c_userid` int(11) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `t_show_week_ranking` (
  `c_id` varchar(80) NOT NULL,
  `c_monday` date NOT NULL,
  `c_sport_steps` int(11) DEFAULT NULL,
  `c_userid` int(11) NOT NULL,
  `c_username` varchar(45) DEFAULT NULL,
  `c_usernick` varchar(45) DEFAULT NULL,
  `c_userhead` varchar(300) DEFAULT NULL,
  `c_collegename` varchar(45) DEFAULT NULL,
  `c_classname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `c_userid` (`c_userid`,`c_monday`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `t_show_month_ranking` (
  `c_id` varchar(80) NOT NULL,
  `c_month` varchar(10) NOT NULL,
  `c_sport_steps` int(11) DEFAULT NULL,
  `c_userid` int(11) NOT NULL,
  `c_username` varchar(45) DEFAULT NULL,
  `c_usernick` varchar(45) DEFAULT NULL,
  `c_userhead` varchar(300) DEFAULT NULL,
  `c_collegename` varchar(45) DEFAULT NULL,
  `c_classname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `c_userid` (`c_userid`,`c_month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;






