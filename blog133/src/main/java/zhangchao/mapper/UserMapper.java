package zhangchao.mapper;

import java.util.List;
import java.util.Map;
import zhangchao.domain.User;

public interface UserMapper {

	int save(User user);
	
	List<User> selectList();
	
	void delete(User u);
}
