package zhangchao.web;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import zhangchao.common.core.R;
import zhangchao.domain.Card;
import zhangchao.domain.User;
import zhangchao.dto.UserAndCardDTO;
import zhangchao.service.UserService;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(method=RequestMethod.GET)
	public R get() {
		List<User> list = this.userService.selectList();
		return R.ok().put("list", list);
	}
	

	
	@RequestMapping(value="/userAndCard", method=RequestMethod.POST)
	public R save(@RequestBody UserAndCardDTO userAndCardDTO){
		// 处理用户
		User user = new User();
		BeanUtils.copyProperties(userAndCardDTO, user);
		user.setId(UUID.randomUUID().toString());
		user.setCreateTime(new Timestamp(System.currentTimeMillis()));
		// 处理卡
		Card card = new Card();
		BeanUtils.copyProperties(userAndCardDTO, card);
		card.setId(UUID.randomUUID().toString());
		card.setCreateTime(new Timestamp(System.currentTimeMillis()));
		card.setUserId(user.getId());
		this.userService.save(user, card);
		return R.ok();
	}
}
