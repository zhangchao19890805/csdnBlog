package zhangchao.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class User {
	
	private String id;
	private String userName;
	private String password;
	private Timestamp createTime;
	private BigDecimal balance;
	
	
	
	
	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public Timestamp getCreateTime() {
		return createTime;
	}




	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}




	public BigDecimal getBalance() {
		return balance;
	}




	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}




	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(" ");
		sb.append(this.id).append(" ");
		sb.append(this.userName).append(" ");
		sb.append(this.password).append(" ");
		sb.append(this.createTime).append(" ");
		sb.append(this.balance).append(" ");
		return sb.toString();
	}
    
}
