package zhangchao.common.exception;

import org.apache.ibatis.session.SqlSession;

/**
 * 统一的异常处理
 * @author 张超
 *
 */
public class ExceptionUtils {
	
	/**
	 * 异常转成字符串
	 * @param t 异常
	 * @return 异常的详细信息的字符串
	 */
	private static String throwable2Str (Throwable t) {
		StackTraceElement[] steArr = t.getStackTrace();
		String[] details = new String[steArr.length];
		for (int i = 0; i < details.length; i++) {
			details[i] = steArr[i].toString();
		}
		String message = t.getMessage();
		StringBuilder content = new StringBuilder();
		for (String str : details) {
			content.append(str).append("\n");
		}
		content.append("\n").append(message).append("\n");
		return content.toString();
	}
	
	/**
	 * 把多个异常合并成一个异常。
	 * @param arr
	 */
	public static RuntimeException join(Throwable[] arr){
		StringBuilder sb = new StringBuilder();
		if (null == arr || arr.length == 0) {
			return null;
		}
		for (int i = 0; i < arr.length; i++) {
			Throwable t = arr[i];
			if (null != t) {
				sb.append(throwable2Str(t)).append("\n");
			}
		}
		return new RuntimeException(sb.toString());
	}
	
	/**
	 * 统一处理异常
	 * @param e
	 */
	public static RuntimeException catchException(Exception e, 
			SqlSession sqlSession_1, boolean sessionCommit_1, Compensate c1,
			SqlSession sqlSession_2, boolean sessionCommit_2, Compensate c2){
		Exception rbEx_1 = null;
		Exception rbEx_2 = null;
		try{
			// 如果User表已经提交，做补偿操作
			if (sessionCommit_1){
				if (null != c1) {
					c1.compensate();
				}
			} else {
				// 如果User表还没提交，回滚
				sqlSession_1.rollback();
				System.out.println("sqlSession.rollback()");
			}
		} catch (Exception ex1) {
			// 这里加入 try ... catch 的原因是，如果 user 表的
			// 数据库停止，上面代码 抛出异常，如果不处理，
			// 后面的代码就无法执行，也就无法回退 sqlSession_2 和包装异常。
			rbEx_1 = ex1;
		}
		try {
			if (sessionCommit_2) {
				if (null != c2) {
					c2.compensate();
				}
			} else {
				sqlSession_2.rollback();
				System.out.println("sqlSession_2.rollback()");
			}
		} catch(Exception ex2) {
			rbEx_2 = ex2;
		}
		RuntimeException r = ExceptionUtils.join(new Exception[]{e, rbEx_1, rbEx_2});
		return r;
	}
	
	public static RuntimeException catchException(Exception e, 
			SqlSession sqlSession_1, boolean sessionCommit_1, Compensate c1,
			SqlSession sqlSession_2){
		RuntimeException r = catchException(e, sqlSession_1, sessionCommit_1, c1,
				sqlSession_2, false, null);
		return r;
	}
}
