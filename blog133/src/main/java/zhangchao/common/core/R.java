package zhangchao.common.core;

import java.util.HashMap;
import java.util.Map;

public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 请求执行成功
     */
    public static final int CODE_SUCCESS = 200;
    /**
     * 参数问题不接受请求，无法使用请求的内容特性来响应请求的网页
     */
    public static final int CODE_REJECT = 406;
    /**
     * 业务条件不满足，服务器未满足请求者在请求中设置的其中一个前提条件
     */
    public static final int CODE_DISCONTENT = 412;
    /**
     * 程序错误，内部服务器错误
     */
    public static final int CODE_ERROR = 500;
    /**
     * 未找到对象
     */
    public static final int CODE_NOT_FOUND = 404;
    /**
     * 拒绝访问
     */
    public static final int CODE_ACCESS_DENIED = 403;
    /**
     * token过期或无效
     * token鉴权失败－没有对应的操作权限
     */
    public static final int CODE_NO_PERMISSION = 401;

    /**
     * 系统异常返回信息
     */
    public static final String MESSAGE_ERROR = "服务器异常,请联系管理员！";

    public R() {
        put("code", CODE_SUCCESS);
        put("msg", "操作成功");
    }

    public R(Integer code, String message, Object data) {
        put("code", code);
        put("msg", message);
        put("data", data);
    }

    public static R ok() {
        return new R();
    }

    public static R data(Object data) {
        return R.ok().put("data", data);
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }


    public static R page(Object page) {
        return R.ok().put("page", page);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R error() {
        return new R(CODE_ERROR, MESSAGE_ERROR, null);
    }

    public static R error(String msg) {
        return error(CODE_ERROR, msg);
    }

    public static R error406(String msg) {
        return error(CODE_REJECT, msg);
    }

    public static R error412(String msg) {
        return error(CODE_DISCONTENT, msg);
    }

    public static R error401() {
        return error(CODE_NO_PERMISSION, "你还没有登录");
    }

    public static R error403() {
        return error(CODE_ACCESS_DENIED, "你没有访问权限");
    }

    public static R error404(Object data) {
        if (data == null) {
            return new R(CODE_NOT_FOUND, "对象不存在", null);
        } else {
            return new R(CODE_SUCCESS, "操作成功", data);
        }

    }

    public static R operate(boolean b) {
        if (b) {
            return R.ok();
        }
        return R.error();
    }

    @Override
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
