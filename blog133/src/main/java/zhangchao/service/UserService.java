package zhangchao.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zhangchao.common.db.FirstDBFactory;
import zhangchao.common.db.SecondDBFactory;
import zhangchao.common.exception.ExceptionUtils;
import zhangchao.dao.CardDao;
import zhangchao.dao.UserDao;
import zhangchao.domain.*;


/**
 * 用户的服务类
 * @author 张超
 *
 */
@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	@Autowired
	private CardDao cardDao;


	public List<User> selectList(){
		SqlSession sqlSession = FirstDBFactory.getInstance().openSession(true);
		List<User> r = null;
		try {
			r = this.userDao.selectList(sqlSession);
		} finally{
			sqlSession.close();
		}
		return r;
	}
	
	/**
	 * 删除用户
	 * @param id
	 */
	public void delete(String id) {
		SqlSession sqlSession = FirstDBFactory.getInstance().openSession(true);
		try {
			this.userDao.delete(sqlSession, id);
		} finally{
			sqlSession.close();
		}
	}
	
	public void save(User user, Card card) {
		// 第一个数据库，放User表
		SqlSession sqlSession_1 = FirstDBFactory.getInstance().openSession();
		// 第二个数据库，放Card表
		SqlSession sqlSession_2 = SecondDBFactory.getInstance().openSession();
		boolean firstSessionCommit = false;
		try {
			this.userDao.save(sqlSession_1, user);
			this.cardDao.save(sqlSession_2, card);
			new BigDecimal("11").divide(user.getBalance(),2);
			
			// 提交
			sqlSession_1.commit();
			firstSessionCommit = true;
			new BigDecimal("11").divide(new BigDecimal(card.getNo()), 2);
			sqlSession_2.commit();
		} catch (Exception e) {
			throw ExceptionUtils.catchException(e, 
					sqlSession_1, firstSessionCommit, ()->{
						this.delete(user.getId());
						System.out.println("调用UserService.delete方法");
					}, 
					sqlSession_2);
		} finally {
			sqlSession_1.close();
			sqlSession_2.close();
		}
	}

}
