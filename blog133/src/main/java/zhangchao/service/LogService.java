package zhangchao.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import zhangchao.common.db.FirstDBFactory;
import zhangchao.dao.LogDao;
import zhangchao.domain.*;

@Service
//@Transactional
public class LogService {
	@Autowired
	private LogDao logDao;
	
	
	public void save(Log log) {
		SqlSession sqlSession = FirstDBFactory.getInstance().openSession();
		try {
			this.logDao.save(sqlSession, log);
		} catch (Exception e) {
			sqlSession.rollback();
		} finally{
			sqlSession.close();
		}
	}

}
