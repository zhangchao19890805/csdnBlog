package zhangchao.dao;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import zhangchao.common.db.FirstDBFactory;
import zhangchao.domain.User;
import zhangchao.mapper.UserMapper;

import org.springframework.stereotype.Component;

@Component
public class UserDao {
	
	public int save(SqlSession sqlSession, User user) {
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		int i = mapper.save(user);
		System.out.println(i);
		return i;
	}

	
	public List<User> selectList(SqlSession sqlSession){
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		return mapper.selectList();
	}

	public void delete(SqlSession sqlSession, String id) {
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		User u = new User();
		u.setId(id);
		mapper.delete(u);
	}
}
