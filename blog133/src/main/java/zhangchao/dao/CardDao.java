package zhangchao.dao;



import org.apache.ibatis.session.SqlSession;

import zhangchao.domain.Card;
import zhangchao.common.db.FirstDBFactory;
import zhangchao.mapper.CardMapper;

import org.springframework.stereotype.Component;

@Component
public class CardDao {
	
	
	public void save(SqlSession sqlSession, Card card) {		
		CardMapper mapper = sqlSession.getMapper(CardMapper.class);
		mapper.save(card);
	}

}
