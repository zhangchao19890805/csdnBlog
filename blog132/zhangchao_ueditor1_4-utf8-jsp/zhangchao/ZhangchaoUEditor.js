function ZhangchaoUEditor(scriptId, config){
    var stringUtils = {
        replaceAll: function (str, oldSubstr, newSbustr) {
            var arr = str.split(oldSubstr);
            if (!arr || arr.length == 0) {
                return str;
            }
            var r = arr[0];
            var i = 1;
            for (; i < arr.length; i++) {
    
                r = r + newSbustr + arr[i];
            }
            return r;
        }
    }
    
    function createDialog(editor, uiName){
        var dialog = new UE.ui.Dialog({
            //指定弹出层中页面的路径
            iframeUrl:'/zhangchao_ueditor1_4-utf8-jsp/zhangchao/code.html',
            //需要指定当前的编辑器实例
            editor:editor,
            //指定dialog的名字
            name:uiName,
            //dialog的标题
            title:"插入代码",
            //指定dialog的外围样式
            cssRules:"width:600px;height:300px;",
            //如果给出了buttons就代表dialog有确定和取消
            buttons:[
                {
                    className:'edui-okbutton',
                    label:'确定',
                    onclick:function () {
                        dialog.close(true);
                    }
                },
                {
                    className:'edui-cancelbutton',
                    label:'取消',
                    onclick:function () {
                        dialog.close(false);
                    }
                }
            ]
        });
        return dialog;
    }

    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    UE.registerUI('ZhangchaoUEditor',function(editor,uiName){
        //创建dialog，因为registerUI第一个参数，只有ZhangchaoUEditor，所以uiName必然是ZhangchaoUEditor
        var dialog = createDialog(editor, "ZhangchaoUEditor");
        //参考addCustomizeButton.js
        var btn = new UE.ui.Button({
            name:'dialogbutton' + uiName,
            title:'插入代码',
            //需要添加的额外样式，指定icon图标，这里默认使用一个重复的icon
            cssRules :'background-position: -440px -40px;',
            onclick:function () {
                //渲染dialog
                dialog.render();
                dialog.open();
            }
        });

        return btn;
    }/*index 指定添加到工具栏上的那个位置，默认时追加到最后,editorId 指定这个UI是那个编辑器实例上的，默认是页面上所有的编辑器都会添加这个按钮*/);
    
    var thisConfig = config || {};
    var instance = UE.getEditor(scriptId, thisConfig);
    
    // begin 绑定UEditor 的ready 事件
    instance.addListener('ready', function() {
        var iframeTag = instance.iframe;
        var win = iframeTag.contentWindow;
        var doc = win.document;

        // 所有代码片段去掉选中样式
        function clearAllCodeBorder(){
            var codeTagArr = doc.querySelectorAll("code.hljs");
            var i = 0;
            var length = codeTagArr.length;
            for(i = 0; i < length; i++){
                var node = codeTagArr[i]
                if (node.className && node.className.indexOf("hljs-zhangchao-codeborder") > -1) {
                    var cn = node.className;
                    cn = stringUtils.replaceAll(cn, "hljs-zhangchao-codeborder", "");
                    node.className = cn;
                }
            };
        }

        // begin 在getContent方法执行之前会触发该事件 
        instance.addListener("beforeGetContent", function(){
            clearAllCodeBorder();
        });
        // end   在getContent方法执行之前会触发该事件 

        // 文档内容 iframe 的点击事件。
        win.onclick = function(e){
            clearAllCodeBorder();
            //把被用户点击的code标签选中
            var node = e.target;
            while(node){
                if (node && node.tagName && node.tagName.toLowerCase() == "code" &&
                        node.className.indexOf("hljs") > -1) {
                    if (node.className.indexOf("hljs-zhangchao-codeborder") < 0) {
                        var cn = node.className;
                        // cn = stringUtils.trim(cn)
                        cn = cn.trim();
                        cn += " hljs-zhangchao-codeborder";
                        node.className = cn;
                    }
                    node = null;
                } else {
                    node = node.parentNode;
                }
            }
        }

        // 文档内容 iframe 的双击事件
        win.ondblclick = function(e){
            clearAllCodeBorder();
            //把被用户点击的code标签选中
            var node = e.target;
            while(node){
                if (node && node.tagName && node.tagName.toLowerCase() == "code" &&
                        node.className.indexOf("hljs") > -1) {
                    if (node.className.indexOf("hljs-zhangchao-codeborder") < 0) {
                        var cn = node.className;
                        // cn = stringUtils.trim(cn)
                        cn = cn.trim();
                        cn += " hljs-zhangchao-codeborder";
                        node.className = cn;
                    }

                    //创建dialog
                    var dialog = createDialog(instance, "ZhangchaoUEditor");
                    dialog.render();
                    dialog.open();
                    node = null;
                } else {
                    node = node.parentNode;
                }
            }
        }
        
        // 文档内容 iframe 的失去焦点事件。
        win.onblur = function(){
            clearAllCodeBorder();
        }

        // 禁止拖拽
        doc.ondragstart = function(e){
            e.preventDefault()
            return false;
        }
    });
    // end 绑定UEditor 的ready 事件
}